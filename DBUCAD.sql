USE [DBUCAD]
GO
ALTER TABLE [dbo].[USUARIOS] DROP CONSTRAINT [FK_USUARIOS_TIPO_USUARIO]
GO
ALTER TABLE [dbo].[UNIVERSIDAD] DROP CONSTRAINT [FK_UNIVERSIDAD_CAMPUS]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] DROP CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_TIPO_USUARIO]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] DROP CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_PUESTO_ADMINISTRATIVO]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] DROP CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_PAIS]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] DROP CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_MUNICIPIO]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] DROP CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_ESTADO_CIVIL]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] DROP CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_DEPARTAMENTO]
GO
ALTER TABLE [dbo].[NOTAS] DROP CONSTRAINT [FK_NOTAS_MATERIAS]
GO
ALTER TABLE [dbo].[NOTAS] DROP CONSTRAINT [FK_NOTAS_ALUMNO]
GO
ALTER TABLE [dbo].[MUNICIPIO] DROP CONSTRAINT [FK_MUNICIPIO_DEPARTAMENTO]
GO
ALTER TABLE [dbo].[MATERIAS] DROP CONSTRAINT [FK_MATERIAS_PENSUM]
GO
ALTER TABLE [dbo].[MATERIAS] DROP CONSTRAINT [FK_MATERIAS_CICLOS]
GO
ALTER TABLE [dbo].[MATERIAS] DROP CONSTRAINT [FK_MATERIAS_CARRERAS]
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO] DROP CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_MATERIAS]
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO] DROP CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_GRUPOS]
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO] DROP CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_ESTADO_MATERIA]
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO] DROP CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_ALUMNO]
GO
ALTER TABLE [dbo].[FACULTADES] DROP CONSTRAINT [FK_FACULTADES_UNIVERSIDAD]
GO
ALTER TABLE [dbo].[DOCUM_X_ALUMNO] DROP CONSTRAINT [FK_DOCUM_X_ALUMNO_TIPO_DOCUMENTOS]
GO
ALTER TABLE [dbo].[DOCUM_X_ALUMNO] DROP CONSTRAINT [FK_DOCUM_X_ALUMNO_ALUMNO]
GO
ALTER TABLE [dbo].[DEPARTAMENTO] DROP CONSTRAINT [FK_DEPARTAMENTO_PAIS]
GO
ALTER TABLE [dbo].[CATEDRATICOS] DROP CONSTRAINT [FK_CATEDRATICOS_TIPO_USUARIO]
GO
ALTER TABLE [dbo].[CATEDRATICOS] DROP CONSTRAINT [FK_CATEDRATICOS_PAIS]
GO
ALTER TABLE [dbo].[CATEDRATICOS] DROP CONSTRAINT [FK_CATEDRATICOS_ESTADO_CIVIL]
GO
ALTER TABLE [dbo].[CATEDRATICOS] DROP CONSTRAINT [FK_CATEDRATICOS_DEPARTAMENTO]
GO
ALTER TABLE [dbo].[CARRERAS] DROP CONSTRAINT [FK_CARRERAS_FACULTADES]
GO
ALTER TABLE [dbo].[CARNET] DROP CONSTRAINT [FK_CARNET_TIPO_CARNET]
GO
ALTER TABLE [dbo].[CARNET] DROP CONSTRAINT [FK_CARNET_ALUMNO]
GO
ALTER TABLE [dbo].[ALUMNO] DROP CONSTRAINT [FK_ALUMNO_TIPO_USUARIO]
GO
ALTER TABLE [dbo].[ALUMNO] DROP CONSTRAINT [FK_ALUMNO_TIPO_INGRESO]
GO
ALTER TABLE [dbo].[ALUMNO] DROP CONSTRAINT [FK_ALUMNO_PAIS]
GO
ALTER TABLE [dbo].[ALUMNO] DROP CONSTRAINT [FK_ALUMNO_MUNICIPIO]
GO
ALTER TABLE [dbo].[ALUMNO] DROP CONSTRAINT [FK_ALUMNO_ESTADO_CIVIL]
GO
ALTER TABLE [dbo].[ALUMNO] DROP CONSTRAINT [FK_ALUMNO_DEPARTAMENTO]
GO
ALTER TABLE [dbo].[ALUMNO] DROP CONSTRAINT [FK_ALUMNO_CARRERAS]
GO
ALTER TABLE [dbo].[ALUMNO] DROP CONSTRAINT [FK_ALUMNO_CAMPUS]
GO
/****** Object:  Table [dbo].[USUARIOS]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[USUARIOS]
GO
/****** Object:  Table [dbo].[UNIVERSIDAD]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[UNIVERSIDAD]
GO
/****** Object:  Table [dbo].[TIPO_USUARIO]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[TIPO_USUARIO]
GO
/****** Object:  Table [dbo].[TIPO_INGRESO]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[TIPO_INGRESO]
GO
/****** Object:  Table [dbo].[TIPO_DOCUMENTOS]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[TIPO_DOCUMENTOS]
GO
/****** Object:  Table [dbo].[TIPO_CARNET]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[TIPO_CARNET]
GO
/****** Object:  Table [dbo].[PUESTO_ADMINISTRATIVO]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[PUESTO_ADMINISTRATIVO]
GO
/****** Object:  Table [dbo].[PERSONAL_ADMINISTRATIVO]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[PERSONAL_ADMINISTRATIVO]
GO
/****** Object:  Table [dbo].[PENSUM]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[PENSUM]
GO
/****** Object:  Table [dbo].[PAIS]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[PAIS]
GO
/****** Object:  Table [dbo].[NOTAS]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[NOTAS]
GO
/****** Object:  Table [dbo].[MUNICIPIO]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[MUNICIPIO]
GO
/****** Object:  Table [dbo].[MATERIAS]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[MATERIAS]
GO
/****** Object:  Table [dbo].[INSCRIPCION_X_ALUMNO]    Script Date: 26/05/2020 16:24:15 ******/
DROP TABLE [dbo].[INSCRIPCION_X_ALUMNO]
GO
/****** Object:  Table [dbo].[GRUPOS]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[GRUPOS]
GO
/****** Object:  Table [dbo].[FACULTADES]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[FACULTADES]
GO
/****** Object:  Table [dbo].[ESTADO_MATERIA]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[ESTADO_MATERIA]
GO
/****** Object:  Table [dbo].[ESTADO_CIVIL]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[ESTADO_CIVIL]
GO
/****** Object:  Table [dbo].[DOCUM_X_ALUMNO]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[DOCUM_X_ALUMNO]
GO
/****** Object:  Table [dbo].[DEPARTAMENTO]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[DEPARTAMENTO]
GO
/****** Object:  Table [dbo].[CICLOS]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[CICLOS]
GO
/****** Object:  Table [dbo].[CATEDRATICOS]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[CATEDRATICOS]
GO
/****** Object:  Table [dbo].[CARRERAS]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[CARRERAS]
GO
/****** Object:  Table [dbo].[CARNET]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[CARNET]
GO
/****** Object:  Table [dbo].[CAMPUS]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[CAMPUS]
GO
/****** Object:  Table [dbo].[ANIO_ELECTIVO]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[ANIO_ELECTIVO]
GO
/****** Object:  Table [dbo].[ALUMNO]    Script Date: 26/05/2020 16:24:16 ******/
DROP TABLE [dbo].[ALUMNO]
GO
USE [master]
GO
/****** Object:  Database [DBUCAD]    Script Date: 26/05/2020 16:24:16 ******/
DROP DATABASE [DBUCAD]
GO
/****** Object:  Database [DBUCAD]    Script Date: 26/05/2020 16:24:16 ******/
CREATE DATABASE [DBUCAD]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DBUCAD', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\DBUCAD.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DBUCAD_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\DBUCAD_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DBUCAD] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBUCAD].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DBUCAD] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBUCAD] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBUCAD] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBUCAD] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBUCAD] SET ARITHABORT OFF 
GO
ALTER DATABASE [DBUCAD] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DBUCAD] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBUCAD] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBUCAD] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBUCAD] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DBUCAD] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBUCAD] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBUCAD] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBUCAD] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBUCAD] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DBUCAD] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBUCAD] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBUCAD] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DBUCAD] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DBUCAD] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBUCAD] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DBUCAD] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DBUCAD] SET RECOVERY FULL 
GO
ALTER DATABASE [DBUCAD] SET  MULTI_USER 
GO
ALTER DATABASE [DBUCAD] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DBUCAD] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DBUCAD] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DBUCAD] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DBUCAD] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DBUCAD', N'ON'
GO
USE [DBUCAD]
GO
/****** Object:  Table [dbo].[ALUMNO]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ALUMNO](
	[alumnoID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](100) NULL,
	[apellido] [nvarchar](100) NULL,
	[sexo] [bit] NULL,
	[estadoCivilID] [int] NULL,
	[direccion] [nvarchar](500) NULL,
	[fechaNacimiento] [date] NULL,
	[telefono] [nvarchar](9) NULL,
	[celular] [nvarchar](9) NULL,
	[correoElectronico] [nvarchar](50) NULL,
	[paisID] [int] NULL,
	[departamentoID] [int] NULL,
	[municipioID] [int] NULL,
	[dui] [nvarchar](11) NULL,
	[nit] [nvarchar](17) NULL,
	[campusID] [int] NULL,
	[tipoUsrID] [int] NOT NULL,
	[fechaMatricula] [date] NULL,
	[tipoIngresoId] [int] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
	[carreraID] [int] NULL,
 CONSTRAINT [PK__ALUMNO__375A03E45D6D3216] PRIMARY KEY CLUSTERED 
(
	[alumnoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ANIO_ELECTIVO]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ANIO_ELECTIVO](
	[anioId] [int] IDENTITY(1,1) NOT NULL,
	[anio] [int] NULL,
	[activo] [bit] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK_ANIO_ELECTIVO] PRIMARY KEY CLUSTERED 
(
	[anioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CAMPUS]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAMPUS](
	[campusID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](100) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK__CAMPUS__D03AB2C02D57E8B7] PRIMARY KEY CLUSTERED 
(
	[campusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CARNET]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CARNET](
	[carnetID] [int] IDENTITY(1,1) NOT NULL,
	[alumnoID] [int] NOT NULL,
	[tipoCarnetId] [int] NOT NULL,
	[carnet] [nvarchar](25) NULL,
	[activo] [bit] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK_CARNET] PRIMARY KEY CLUSTERED 
(
	[carnetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CARRERAS]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CARRERAS](
	[carreraID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](150) NULL,
	[codigo] [nvarchar](10) NULL,
	[facultadesID] [int] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK__CARRERAS__C837724BCB497011] PRIMARY KEY CLUSTERED 
(
	[carreraID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CATEDRATICOS]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CATEDRATICOS](
	[catedraticoID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](100) NULL,
	[apellido] [nvarchar](100) NULL,
	[sexo] [bit] NULL,
	[estadoCivilID] [int] NULL,
	[direccion] [nvarchar](500) NULL,
	[fechaNacimiento] [date] NULL,
	[telefono] [nvarchar](9) NULL,
	[celular] [nvarchar](9) NULL,
	[correoElectronico] [nvarchar](50) NULL,
	[paisID] [int] NULL,
	[departamentoID] [int] NULL,
	[municipioID] [int] NULL,
	[dui] [nvarchar](11) NULL,
	[nit] [nvarchar](16) NULL,
	[campusID] [int] NULL,
	[tipoUsrID] [int] NOT NULL,
	[materiaID] [int] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK__CATEDRAT__3AC9AF7BDC717AE3] PRIMARY KEY CLUSTERED 
(
	[catedraticoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CICLOS]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CICLOS](
	[cicloID] [int] IDENTITY(1,1) NOT NULL,
	[codigo] [nvarchar](10) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK__CICLOS__D452528D57C00DBF] PRIMARY KEY CLUSTERED 
(
	[cicloID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DEPARTAMENTO]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DEPARTAMENTO](
	[departamentoID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](100) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
	[paisId] [int] NOT NULL,
 CONSTRAINT [PK__DEPARTAM__A67AC1588A3DA21F] PRIMARY KEY CLUSTERED 
(
	[departamentoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DOCUM_X_ALUMNO]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DOCUM_X_ALUMNO](
	[docalumId] [int] IDENTITY(1,1) NOT NULL,
	[documentoId] [int] NULL,
	[alumnoID] [int] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
	[entregado] [bit] NULL,
 CONSTRAINT [PK_DOCUM_X_ALUMNO] PRIMARY KEY CLUSTERED 
(
	[docalumId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ESTADO_CIVIL]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ESTADO_CIVIL](
	[estadoCivilID] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](50) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
	[codEstado] [nvarchar](20) NULL,
 CONSTRAINT [PK__ESTADO_C__FF92122F3820E729] PRIMARY KEY CLUSTERED 
(
	[estadoCivilID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ESTADO_MATERIA]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ESTADO_MATERIA](
	[estaMateriaID] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK_ESTADO_MATERIA] PRIMARY KEY CLUSTERED 
(
	[estaMateriaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FACULTADES]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FACULTADES](
	[facultadesID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](150) NULL,
	[codigo] [nvarchar](10) NULL,
	[universidadID] [int] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK__FACULTAD__E6249CDEF868B45F] PRIMARY KEY CLUSTERED 
(
	[facultadesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GRUPOS]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GRUPOS](
	[grupoID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](10) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK__GRUPOS__A0BD40FF6A15FDC2] PRIMARY KEY CLUSTERED 
(
	[grupoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[INSCRIPCION_X_ALUMNO]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[INSCRIPCION_X_ALUMNO](
	[insalumID] [int] IDENTITY(1,1) NOT NULL,
	[alumnoID] [int] NOT NULL,
	[materiaID] [int] NOT NULL,
	[estaMateriaID] [int] NOT NULL,
	[grupoID] [int] NOT NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK_INSCRIPCION_X_ALUMNO] PRIMARY KEY CLUSTERED 
(
	[insalumID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MATERIAS]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MATERIAS](
	[materiaID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](150) NULL,
	[codigo] [nvarchar](10) NULL,
	[carreraID] [int] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
	[pensumID] [int] NULL,
	[cicloID] [int] NULL,
 CONSTRAINT [PK__MATERIAS__3BC06EBA0CC9E4CA] PRIMARY KEY CLUSTERED 
(
	[materiaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MUNICIPIO]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MUNICIPIO](
	[municipioID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](100) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
	[departamentoID] [int] NOT NULL,
 CONSTRAINT [PK__MUNICIPI__CBC36A2133ADEE64] PRIMARY KEY CLUSTERED 
(
	[municipioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NOTAS]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NOTAS](
	[notaID] [int] IDENTITY(1,1) NOT NULL,
	[idMateria] [int] NULL,
	[notaUno] [decimal](18, 2) NULL,
	[notaDos] [decimal](18, 2) NULL,
	[notaTres] [decimal](18, 2) NULL,
	[notaCuatro] [decimal](18, 2) NULL,
	[notaCinco] [decimal](18, 2) NULL,
	[notaSeis] [decimal](18, 2) NULL,
	[notaSiete] [decimal](18, 2) NULL,
	[notaOcho] [decimal](18, 2) NULL,
	[notaNueve] [decimal](18, 2) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
	[alumnoID] [int] NULL,
 CONSTRAINT [PK__NOTAS__0ADFDACACC34AB09] PRIMARY KEY CLUSTERED 
(
	[notaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PAIS]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PAIS](
	[paisID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](100) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
	[nacionalidad] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK__PAIS__45785BAB7F065139] PRIMARY KEY CLUSTERED 
(
	[paisID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PENSUM]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PENSUM](
	[pensumID] [int] IDENTITY(1,1) NOT NULL,
	[carreraID] [int] NOT NULL,
	[nombre] [varchar](100) NULL,
	[anio] [int] NULL,
	[activo] [bit] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK_PENSUM] PRIMARY KEY CLUSTERED 
(
	[pensumID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PERSONAL_ADMINISTRATIVO]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PERSONAL_ADMINISTRATIVO](
	[personalAdminID] [int] IDENTITY(1,1) NOT NULL,
	[puestoID] [int] NULL,
	[nombre] [nvarchar](100) NULL,
	[apellido] [nvarchar](100) NULL,
	[sexo] [bit] NULL,
	[estadoCivilID] [int] NULL,
	[direccion] [nvarchar](500) NULL,
	[fechaNacimiento] [date] NULL,
	[telefono] [nvarchar](9) NULL,
	[celular] [nvarchar](9) NULL,
	[correoElectronico] [nvarchar](50) NULL,
	[paisID] [int] NULL,
	[departamentoID] [int] NULL,
	[municipioID] [int] NULL,
	[dui] [nvarchar](11) NULL,
	[nit] [nvarchar](16) NULL,
	[tipoUsrID] [int] NOT NULL,
	[campusID] [int] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK__PERSONAL__27515FB067454BBF] PRIMARY KEY CLUSTERED 
(
	[personalAdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PUESTO_ADMINISTRATIVO]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PUESTO_ADMINISTRATIVO](
	[puestoID] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](200) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK__PUESTO_A__2BAB30826B8EB4FD] PRIMARY KEY CLUSTERED 
(
	[puestoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TIPO_CARNET]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIPO_CARNET](
	[tipoCarnetId] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](50) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK_TIPO_CARNET] PRIMARY KEY CLUSTERED 
(
	[tipoCarnetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TIPO_DOCUMENTOS]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TIPO_DOCUMENTOS](
	[documentoId] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK_TIPO_DOCUMENTOS] PRIMARY KEY CLUSTERED 
(
	[documentoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TIPO_INGRESO]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TIPO_INGRESO](
	[tipoIngresoId] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](100) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
	[codTipIngreso] [nvarchar](10) NULL,
 CONSTRAINT [PK__TIPO_ING__CAF27F1BDB056BA4] PRIMARY KEY CLUSTERED 
(
	[tipoIngresoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TIPO_USUARIO]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TIPO_USUARIO](
	[tipoUsrID] [int] IDENTITY(1,1) NOT NULL,
	[nombreTipo] [nvarchar](100) NULL,
	[descripcion] [nvarchar](500) NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
	[codTipUsr] [varchar](5) NULL,
 CONSTRAINT [PK__TIPO_USU__12583A4384546ED0] PRIMARY KEY CLUSTERED 
(
	[tipoUsrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UNIVERSIDAD]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UNIVERSIDAD](
	[universidadID] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](150) NULL,
	[direccion] [nvarchar](150) NULL,
	[telefono] [nvarchar](9) NULL,
	[campusID] [int] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK__UNIVERSI__77BB8896034860AD] PRIMARY KEY CLUSTERED 
(
	[universidadID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[USUARIOS]    Script Date: 26/05/2020 16:24:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[USUARIOS](
	[usuarioID] [int] IDENTITY(1,1) NOT NULL,
	[nombreUSR] [nvarchar](20) NOT NULL,
	[contrasena] [nvarchar](50) NOT NULL,
	[tipoUsrID] [int] NOT NULL,
	[activo] [bit] NULL,
	[fechaAdicion] [date] NULL,
	[adicionadoPor] [nvarchar](20) NULL,
	[fechaModificacion] [date] NULL,
	[modificadoPor] [nvarchar](20) NULL,
 CONSTRAINT [PK__USUARIOS__A5B1ABAEB7C50F75] PRIMARY KEY CLUSTERED 
(
	[usuarioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[ALUMNO] ON 

INSERT [dbo].[ALUMNO] ([alumnoID], [nombre], [apellido], [sexo], [estadoCivilID], [direccion], [fechaNacimiento], [telefono], [celular], [correoElectronico], [paisID], [departamentoID], [municipioID], [dui], [nit], [campusID], [tipoUsrID], [fechaMatricula], [tipoIngresoId], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [carreraID]) VALUES (1, N'Boris Enrique', N'Alfaro López', 0, 1, N'calle antigua Zacamil, calle principal #3, mejicanos', CAST(N'1989-10-01' AS Date), N'2528-1816', N'7589-3627', N'be.alfaro@outlook.com', 21, 6, 178, N'04166811-8', N'0604-011089-105-2', 1, 4, CAST(N'2015-01-17' AS Date), 2, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 6)
SET IDENTITY_INSERT [dbo].[ALUMNO] OFF
SET IDENTITY_INSERT [dbo].[ANIO_ELECTIVO] ON 

INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, 1983, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (2, 1984, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, 1985, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (4, 1986, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (5, 1987, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (6, 1988, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (7, 1989, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (8, 1990, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (9, 1991, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (10, 1992, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (11, 1993, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (12, 1994, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (13, 1995, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (14, 1996, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (15, 1997, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (16, 1998, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (17, 1999, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (18, 2000, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (19, 2001, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (20, 2002, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (21, 2003, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (22, 2004, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (23, 2005, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (24, 2006, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (25, 2007, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (26, 2008, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (27, 2009, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (28, 2010, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (29, 2011, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (30, 2012, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (31, 2013, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (32, 2014, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (33, 2015, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (34, 2016, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (35, 2017, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (36, 2018, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (37, 2019, 0, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ANIO_ELECTIVO] ([anioId], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (38, 2000, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[ANIO_ELECTIVO] OFF
SET IDENTITY_INSERT [dbo].[CAMPUS] ON 

INSERT [dbo].[CAMPUS] ([campusID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, N'CENTRAL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[CAMPUS] OFF
SET IDENTITY_INSERT [dbo].[CARNET] ON 

INSERT [dbo].[CARNET] ([carnetID], [alumnoID], [tipoCarnetId], [carnet], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, 1, 3, N'AL15001', 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[CARNET] OFF
SET IDENTITY_INSERT [dbo].[CARRERAS] ON 

INSERT [dbo].[CARRERAS] ([carreraID], [nombre], [codigo], [facultadesID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, N'Licenciatura en Administración de Empresas', N'ADMONEM', 1, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CARRERAS] ([carreraID], [nombre], [codigo], [facultadesID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (2, N'Ingeniería en Ciencias de la Computación', N'INGCOM', 1, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CARRERAS] ([carreraID], [nombre], [codigo], [facultadesID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, N'Licenciatura en Contaduría Pública', N'CONTAPU', 1, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CARRERAS] ([carreraID], [nombre], [codigo], [facultadesID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (4, N'Licenciatura en Ciencias de la Comunicación', N'CIENCOMU', 2, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CARRERAS] ([carreraID], [nombre], [codigo], [facultadesID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (5, N'Profesorado y Licenciatura en Educación Inicial y Parvularia', N'EDUINIPAR', 2, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CARRERAS] ([carreraID], [nombre], [codigo], [facultadesID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (6, N'Licenciatura en Teología con especialidad en Misionología', N'TEOMISI', 3, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CARRERAS] ([carreraID], [nombre], [codigo], [facultadesID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (7, N'Licenciatura en Ciencias Jurídicas', N'CIENJU', 4, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[CARRERAS] OFF
SET IDENTITY_INSERT [dbo].[CICLOS] ON 

INSERT [dbo].[CICLOS] ([cicloID], [codigo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, N'I', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CICLOS] ([cicloID], [codigo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (2, N'II', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CICLOS] ([cicloID], [codigo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, N'III', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CICLOS] ([cicloID], [codigo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (4, N'IV', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CICLOS] ([cicloID], [codigo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (5, N'V', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CICLOS] ([cicloID], [codigo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (6, N'VI', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CICLOS] ([cicloID], [codigo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (7, N'VII', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CICLOS] ([cicloID], [codigo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (8, N'VIII', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CICLOS] ([cicloID], [codigo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (9, N'IX', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[CICLOS] ([cicloID], [codigo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (10, N'X', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[CICLOS] OFF
SET IDENTITY_INSERT [dbo].[DEPARTAMENTO] ON 

INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (1, N'AHUACHAPAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (2, N'SANTA ANA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (3, N'SONSONATE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (4, N'CHALATENANGO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (5, N'LA LIBERTAD', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (6, N'SAN SALVADOR', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (7, N'CUSCATLAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (8, N'LA PAZ', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (9, N'CABA?AS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (10, N'SAN VICENTE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (11, N'USULUTAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (12, N'SAN MIGUEL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (13, N'MORAZAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
INSERT [dbo].[DEPARTAMENTO] ([departamentoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [paisId]) VALUES (14, N'LA UNION', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 21)
SET IDENTITY_INSERT [dbo].[DEPARTAMENTO] OFF
SET IDENTITY_INSERT [dbo].[DOCUM_X_ALUMNO] ON 

INSERT [dbo].[DOCUM_X_ALUMNO] ([docalumId], [documentoId], [alumnoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [entregado]) VALUES (1, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[DOCUM_X_ALUMNO] ([docalumId], [documentoId], [alumnoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [entregado]) VALUES (2, 2, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[DOCUM_X_ALUMNO] ([docalumId], [documentoId], [alumnoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [entregado]) VALUES (3, 3, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[DOCUM_X_ALUMNO] ([docalumId], [documentoId], [alumnoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [entregado]) VALUES (4, 4, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[DOCUM_X_ALUMNO] ([docalumId], [documentoId], [alumnoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [entregado]) VALUES (5, 5, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[DOCUM_X_ALUMNO] ([docalumId], [documentoId], [alumnoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [entregado]) VALUES (6, 6, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[DOCUM_X_ALUMNO] ([docalumId], [documentoId], [alumnoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [entregado]) VALUES (7, 7, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[DOCUM_X_ALUMNO] OFF
SET IDENTITY_INSERT [dbo].[ESTADO_CIVIL] ON 

INSERT [dbo].[ESTADO_CIVIL] ([estadoCivilID], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codEstado]) VALUES (1, N'Casado(a)', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'C')
INSERT [dbo].[ESTADO_CIVIL] ([estadoCivilID], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codEstado]) VALUES (2, N'Divorciado(a)', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'D')
INSERT [dbo].[ESTADO_CIVIL] ([estadoCivilID], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codEstado]) VALUES (3, N'Soltero(a)', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'S')
INSERT [dbo].[ESTADO_CIVIL] ([estadoCivilID], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codEstado]) VALUES (4, N'Viudo(a)', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'V')
INSERT [dbo].[ESTADO_CIVIL] ([estadoCivilID], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codEstado]) VALUES (5, N'Comprometido(a)', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'P')
SET IDENTITY_INSERT [dbo].[ESTADO_CIVIL] OFF
SET IDENTITY_INSERT [dbo].[ESTADO_MATERIA] ON 

INSERT [dbo].[ESTADO_MATERIA] ([estaMateriaID], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, N'Aprobada', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ESTADO_MATERIA] ([estaMateriaID], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (2, N'Reprobada', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[ESTADO_MATERIA] ([estaMateriaID], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, N'Cursando', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[ESTADO_MATERIA] OFF
SET IDENTITY_INSERT [dbo].[FACULTADES] ON 

INSERT [dbo].[FACULTADES] ([facultadesID], [nombre], [codigo], [universidadID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, N'Ciencias Económicas', N'CIEC', 1, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[FACULTADES] ([facultadesID], [nombre], [codigo], [universidadID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (2, N'Ciencias y Humanidades', N'CIHU', 1, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[FACULTADES] ([facultadesID], [nombre], [codigo], [universidadID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, N'Teología', N'TEOG', 1, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[FACULTADES] ([facultadesID], [nombre], [codigo], [universidadID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (4, N'Jurisprudencia y Ciencias Sociales', N'CIJU', 1, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[FACULTADES] OFF
SET IDENTITY_INSERT [dbo].[GRUPOS] ON 

INSERT [dbo].[GRUPOS] ([grupoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, N'Grupo 1', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[GRUPOS] ([grupoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (2, N'Grupo 2', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[GRUPOS] ([grupoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, N'Grupo 3', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[GRUPOS] ([grupoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (4, N'Grupo 4', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[GRUPOS] ([grupoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (5, N'Grupo 5', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[GRUPOS] ([grupoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (6, N'Grupo 6', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[GRUPOS] ([grupoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (7, N'Grupo 7', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[GRUPOS] ([grupoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (8, N'Grupo 8', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[GRUPOS] ([grupoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (9, N'Grupo 9', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[GRUPOS] ([grupoID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (10, N'Grupo 10', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[GRUPOS] OFF
SET IDENTITY_INSERT [dbo].[INSCRIPCION_X_ALUMNO] ON 

INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, 1, 1, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (2, 1, 2, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, 1, 3, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (4, 1, 4, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (5, 1, 5, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (6, 1, 6, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (7, 1, 7, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (8, 1, 8, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (9, 1, 9, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (10, 1, 10, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (11, 1, 11, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (12, 1, 12, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (13, 1, 13, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (14, 1, 14, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (15, 1, 15, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (16, 1, 16, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (17, 1, 17, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (18, 1, 18, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (19, 1, 19, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (20, 1, 20, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (21, 1, 21, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (22, 1, 22, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (23, 1, 23, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (24, 1, 24, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (25, 1, 25, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (26, 1, 26, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (27, 1, 27, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (28, 1, 28, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (29, 1, 29, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (30, 1, 31, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (31, 1, 32, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (32, 1, 33, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (33, 1, 34, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (34, 1, 35, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (35, 1, 36, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (36, 1, 37, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (37, 1, 38, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (38, 1, 39, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (39, 1, 40, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (40, 1, 43, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (41, 1, 44, 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[INSCRIPCION_X_ALUMNO] ([insalumID], [alumnoID], [materiaID], [estaMateriaID], [grupoID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (42, 1, 11, 2, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[INSCRIPCION_X_ALUMNO] OFF
SET IDENTITY_INSERT [dbo].[MATERIAS] ON 

INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (1, N'INTRODUCCION AL ESTUDIO BIBLICO Y TEOLOGICO', N'BBTE201', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (2, N'ESTADISTICA I', N'ESTA1', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (3, N'GRAMATICA Y TECNICAS DE REDACCIÓN', N'GRTR102', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (4, N'INTRODUCCION A LAS MISIONES', N'INTMI', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (5, N'FILOSOFIA GENERAL', N'FILGE', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (6, N'INTRODUCCION AL CRISTIANISMO', N'INTRCRI', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (7, N'METODOS Y TECNICAS DE INVESTIGACION I', N'METEC1', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (8, N'ANTROPOLOGIA CULTURAL', N'ANTROC', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (9, N'LINGÜÍSTICA', N'LINGUI', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (10, N'ETICA Y DESARROLLO PROFESIONAL', N'ETDESA', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (11, N'TEOLOGIA I', N'TEO1', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (12, N'METODOS Y TECNICAS DE INVESTIGACION II', N'METEC2', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (13, N'HERMENEUTICA', N'HERME', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (14, N'INGLES I', N'INGL1', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (15, N'INFORMATICA I', N'INFO1', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (16, N'TEOLOGIA II ', N'TEO2', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (17, N'HISTORIA DE LA EXPANSION DE LA IGLESIA', N'HISTEIG', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (18, N'CULTURA BIBLICA', N'CULBIB', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (19, N'INGLES II', N'INGL2', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (20, N'INFORMATICA II', N'INFO2', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (21, N'TEOLOGIA III', N'TEO3', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (22, N'ESTRATEGIAS PARA EL EVANGELISMO MUNDIAL', N'ESTEVMU', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (23, N'PANORAMA DEL ANTIGUO TESTAMENTO', N'PANANTE', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (24, N'GRIEGO DEL NUEVO TESTAMENTO', N'GRINT', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (25, N'TEORIA DE LA COMUNICACIÓN', N'TEORCOM', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (26, N'TEOLOGIA CONTEMPORANEA', N'TEOCOM', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (27, N'RELIGIONES DEL MUNDO', N'RELMUN', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (28, N'PENTATEUCO', N'PENTA', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (29, N'NUESTRO NUEVO TESTAMENTO', N'NUNUTE', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (30, N'LOGISTICA MISIONERA', N'LOGMIS', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (31, N'TEOLOGIA MINISTERIAL', N'TEOMINS', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (32, N'MODELOS DE CAPACITACION MISIONERA', N'MODCAP', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (33, N'LIBROS HISTORICOS', N'LIBHIST', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (34, N'VIDA Y ENSEÑANZA DE JESUS', N'VENSJE', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (35, N'COMUNICACIÓN TRANSCULTURAL DEL EVANGELIO', N'COMTRAE', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (36, N'APOLOGETICA ', N'APOLOG', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (37, N'TEOLOGIA DE LA IGLESIA Y SU MISION ', N'TEOIMI', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (38, N'PROFETAS MAYORES', N'PROMAY', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (39, N'VIDA Y ESCRITOS DE PABLO ', N'VIESPA', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (40, N'PRACTICA MINISTERIAL NACIONAL', N'PRACMI', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (41, N'ADMINISTRACION ECLESIASTICA', N'ADMONE', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (42, N'HOMILETICA', N'HOMILE', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (43, N'EPISTOLAS GENERALES', N'EPISTOL', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (44, N'PRACTICA MISIONERA TRANSCULTURAL', N'PRACMI2', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (45, N'LIDERAZGO', N'LIDER', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (46, N'ELECTIVA MISIONOLOGICA', N'ELECMISIO', 6, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 1, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (47, N'INTRODUCCION AL ESTUDIO BÍBLICO Y TEOLÓGICO', N'IBTE', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (48, N'ESTADIÍSTICA', N'ESTA ', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (49, N'GRAMATICA Y TECNICAS DE REDACCIÓN', N'GYTR', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (50, N'TEORIA GERENCIAL I', N'TEJE', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (51, N'MATEMATICA I', N'MATE', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (52, N'METODOS Y TECNICAS DE INVESTIGACION I', N'MYTI', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (53, N'ESTADISTICA II', N'ESTAII', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (54, N'ETICA Y DESARROLLO PROFESIONAL', N'ETDP', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (55, N'TEORIA GERENCIAL II', N'TEGEII', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (56, N'MATEMATICA II', N'MATEII', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (57, N'METODOS Y TECNICAS DE INVESTIGACION II', N'MYTII', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (58, N'INFORMATICA I', N'INFO', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (59, N'INGLES I', N'INGLEI', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (60, N'CONTABILIDAD I', N'CONTI', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (61, N'INTRODUCCION A LA ECONONIA', N'INEC', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (62, N'LEGISLACION EMPRESARIAL', N'LEEM', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (63, N'INFORMATICA II', N'INFOII', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (64, N'INGLES II', N'INGLII', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (65, N'CONTABILIDAD II', N'CONTII', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (66, N'MICROECONOMIA', N'MICR', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (67, N'GESTION DE MERCADEO', N'GEME', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (68, N'PSICOLOGIA EMPRESARIAL', N'PSEM', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (69, N'GESTION DE TALENTO HUMANO I', N'GETH', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (70, N'CONTABILIDAD DE COSTROS', N'COCO', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (71, N'MACROECONOMIA', N'MACR', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (72, N'INVESTIGACION DE MERCADO', N'INME', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (73, N'MATEMATICA FINANCIERA', N'MAFI', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (74, N'GESTION DEL TALENTO HUMANO II', N'GETHII', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (75, N'EMPRENDEDURISMO', N'EMPR', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (76, N'FUNDAMENTO DE ANALISIS FINANCIERO', N'FDAF', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (77, N'GESTION MEDIO AMBIENTE', N'GEMA', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (78, N'ORGANIZACIÓN Y METODOS', N'OYME', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (79, N'GESETION FINANCIERA A CORTO PLAZO', N'GFCP', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (80, N'PRESUPUESTOS', N'PREP', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (81, N'GESTION DE LA CALIDAD', N'GECA', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (82, N'ADMINISTRACION DE MYPES', N'AMPE', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (83, N'ADMINISTRACION PUBLICA', N'ADPU', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (84, N'GESTION FINANCIERA A LARGO PLAZO', N'GFLP', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (85, N'DESARROLLO ECONOMICO Y SOCIAL', N'DEYS', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (86, N'INVESTIGACION DE OPERACIONES', N'INDO', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (87, N'COMERCIO INTERNACIONAL', N'COIN', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (88, N'CONSULTORIA ADMINISTRATIVA ', N'COAD', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (89, N'CONTABILIDAD GERENCIAL', N'COGE', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (90, N'ADMINISTRACION DE OPERACIONES', N'ADOP', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (91, N'GERENCIA ESTRATEGICA', N'GEES', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (92, N'SISTEMAS DE INFORMACION GERENCIAL', N'SIGE', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (93, N'FORMACION, EVALUACION Y GESTION DE PROYECTOS', N'FEGP', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (94, N'SEMINARIO DE ALTA GERENCIA', N'SAGE', 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 3, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (95, N'INTRODUCCION AL ESTUDIO BÍBLICO Y TEOLÓGICO', N'IBTE', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (96, N'ESTADIÍSTICA', N'ESTA ', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (97, N'GRAMATICA Y TECNICAS DE REDACCIÓN', N'GYTR', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (98, N'INFORMATICA I', N'INFO', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (99, N'METODOS Y TECNICAS DE INVESTIGACION I', N'MYTI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 1)
GO
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (100, N'FISICA', N'FISI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (101, N'LOGICA COMPUTACIONAL', N'LOGC', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (102, N'MATEMATICA II', N'MATEII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (103, N'INFORMATICA II', N'INFOII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (104, N'METODOS Y TECNICAS DE INVESTIGACION II', N'MYTII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (105, N'FISICA II', N'FISII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (106, N'PROGRAMACION I', N'PROGI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (107, N'MATEMATICA III', N'MATEII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (108, N'INGLES I', N'INGLEI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (109, N'MANTENIMIENTO DE COMPUTADORAS', N'MACO', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (110, N'ELECTRICIDAD Y ELECTRONICA', N'ELEE', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (111, N'PROGRAMACION II', N'PROGII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (112, N'MATEMATICAS IV', N'MATEIV', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (113, N'INGLES II', N'INGLII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (114, N'SOFTWARE LIBRE', N'SOLI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (115, N'SISTEMAS ELECTRONICOS Y DIGITALES', N'SIED', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (116, N'PROGRAMACION III', N'PROGIII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (117, N'ANALISIS NUMERICO', N'ANNU', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (118, N'INGENIERIA ECONOMICA', N'INEC', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (119, N'DISEÑO ASISTIDO POR COMPUTADORAS I', N'DICOI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (120, N'ETICA Y DESARROLLO PROFESIONAL', N'ETDP', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (121, N'ESTRUCTURAS DE DATOS', N'ESDA', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (122, N'BASE DE DATOS I', N'BADA', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (123, N'CONTABILIDAD Y COSTRO', N'CCOS', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (124, N'DISEÑO ASISTIDO POR COMPUTADORAS II', N'DICOII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (125, N'TEORIA GERENCIAL I', N'TEGEI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (126, N'SISTEMAS OPERATIVOS I', N'SIOPI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (127, N'BASE DE DATOS II', N'BADAII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (128, N'ANALISIS Y DISEÑO DE SISTEMAS', N'ADSI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (129, N'LEGISLACION EMPRESARIAL', N'LEEM', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (130, N'INGENIERIA DE SOFTWARE', N'INSO', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (131, N'SISTEMAS OPERATIVOS II', N'SIOPII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (132, N'REDES DE COMPUTADORAS I', N'RECOI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (133, N'AUDITORIA INFORMATICA', N'AUIN', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (134, N'SISTEMA DE INFORMACION GERENCIAL', N'SIIN', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (135, N'PROGRAMACION WEB I', N'PROWI', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (136, N'SEGURIDAD INFORMATICA', N'SEIN', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (137, N'REDES DE COMPUTADORAS II', N'RECOII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (138, N'ADMINISTRACION PROYECTOS DE INFORMATICA', N'APIN', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (139, N'PROGRAMACION WEB II', N'PROWII', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (140, N'ADMINISTRACION DE CENTROS DE COMPUTO', N'ADCC', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (141, N'INTRODUCCION AL ESTUDIO BÍBLICO Y TEOLÓGICO', N'IBTE', 2, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 4, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (142, N'ESTADIÍSTICA', N'ESTA ', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (143, N'GRAMATICA Y TECNICAS DE REDACCIÓN', N'GYTR', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (144, N'TEORIA GERENCIAL I', N'TEJE', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (145, N'MATEMATICA I', N'MATE', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (146, N'METODOS Y TECNICAS DE INVESTIGACION I', N'MYTI', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (147, N'ESTADISTICA II', N'ESTAII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (148, N'ETICA Y DESARROLLO PROFESIONAL', N'ETDP', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (149, N'TEORIA GERENCIAL II', N'TEGEII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (150, N'MATEMATICA II', N'MATEII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (151, N'METODOS Y TECNICAS DE INVESTIGACION II', N'MYTII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (152, N'INFORMATICA I', N'INFO', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (153, N'INGLES I', N'INGLEI', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (154, N'CONTABILIDAD I', N'CONTI', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (155, N'INTRODUCCION A LA ECONONIA', N'INEC', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (156, N'LEGISLACION EMPRESARIAL', N'LEEM', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (157, N'INFORMATICA II', N'INFOII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (158, N'INGLES II', N'INGLII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (159, N'CONTABILIDAD II', N'CONTII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (160, N'MICROECONOMIA', N'MICR', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (161, N'LEGISLACION EMPRESARIAL II', N'LEEMII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (162, N'CONTABILIDAD DE COSTROS I', N'COCOI', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (163, N'CONTABILIDAD FISCAL', N'COFI', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (164, N'MACROECONOMIA', N'MACR', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (165, N'CONTABILIDAD III', N'CONTIII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (166, N'MATEMATICA FINANCIERA', N'MAFI', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (167, N'CONTABILIDAD DE COSTO II', N'COCOII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (168, N'FUNDAMENTO DE ANALISIS FINANCIERO', N'FDAF', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (169, N'DERECHO TRIBUTARIO FINANCIERO', N'DTFI', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (170, N'CONTABILIDAD IV', N'CONTIV', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (171, N'COMERCIO INTERNACIONAL', N'COIN', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (172, N'PRESUPUESTOS', N'PREP', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (173, N'GESTION FINANCIERA A CORTO PLAZO', N'GFCP', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (174, N'SISTEMAS CONTABLES COMPUTARIZADOS', N'SICC', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (175, N'CONTABILIDAD V', N'CONTV', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (176, N'CONTABILIDAD AGROPECUARIO E INDUSTRIAL', N'COAI', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (177, N'CONTABILIDAD GUBERNAMENTAL', N'COGU', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (178, N'GESTION FINANCIERA A LARGO PLAZO', N'GFLP', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (179, N'CONTABILIDAD DE INSTITUCIONES DE SEGUROS', N'COIS', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (180, N'AUDITORIA FINANCIERA I', N'AUFI', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (181, N'AUDITORIA GUBERNAMENTAL', N'AUGU', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (182, N'AUDITORIA INTERNA Y CONTRALORIA', N'AICO', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (183, N'CONTABILIDAD DE BANCOS Y FINANCIEROAS', N'COBF', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (184, N'AUDITORIA FINANCIERA II', N'AUFII', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (185, N'TALLER SUPERIOR DE ORGANIZACIÓN PRACTICA CONTABLE', N'TSPC', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (186, N'AUDITORIA INFORMATICA', N'AUIN', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (187, N'AUDITORIA FISCAL', N'AFIS', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (188, N'SEMINARIO DE AUDITORIA', N'SEAU', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (189, N'INTRODUCCION AL ESTUDIO BÍBLICO Y TEOLÓGICO', N'IBTE', 3, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 5, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (190, N'ESTADIÍSTICA', N'ESTA ', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (191, N'GRAMATICA Y TECNICAS DE REDACCIÓN', N'GYTR', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (192, N'SOCIOLOGIA GENERAL', N'SOGE', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (193, N'TEORIA DE LA COMUNICAICON', N'TECO', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (194, N'ETICA Y DESARROLLO PROFESIONAL', N'ETDP', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (195, N'METODOS Y TECNICAS DE INVESTIGACIONI', N'MYTI', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (196, N'REDACCION PARA MEDIOS', N'REME', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (197, N'SOCIOLOGIA DE LA COMUNICACIÓN', N'SOCO', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (198, N'MEDIOS DE COMUNICACIÓN', N'MECO', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (199, N'INFORMATICA I', N'INFO', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 2)
GO
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (200, N'METODOS Y TECNICAS DE INVESTIGACION II', N'MYTII', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (201, N'REALIDAD NACIONAL', N'REAN', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (202, N'PSICOLOGIA DE LA COMUNICACIÓN', N'PCOM', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (203, N'EXPRESION VERBAL', N'EXVE', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (204, N'DISEÑO WEB', N'DEWE', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (205, N'INGLES INTENSIVO I', N'IINT', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (206, N'LEGISLACION PARA LOS MEDIOS', N'LEME', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (207, N'TALLER DE CREATIVIDAD', N'TACR', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (208, N'TECNICAS DE AUDIO', N'TEDA', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (209, N'COMUNICACIÓN Y TECNOLOGIA', N'COMT', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (210, N'INGLES INTENSIVO II', N'IINTII', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (211, N'FOTOGRAFIA I', N'FOTO', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (212, N'TECNICAS DE VIDEO', N'TEDV', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (213, N'TALLER DE AUDIO II', N'TEDAII', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (214, N'TALLER DE EXPRESION PLASTICA', N'TEXP', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (215, N'RELACIONES PUBLICAS', N'RELP', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (216, N'FOTOGRAFIA II', N'FOTOII', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (217, N'TALLER DE VIDEO II', N'TADVII', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (218, N'PRACTICA PROFESIONAL DE AUDIO', N'PRPA', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (219, N'DISEÑO MULTIMEDIA', N'DEMU', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (220, N'PROTOCOLO I', N'PROT', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (221, N'PRENSA', N'PREN', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (222, N'PRACTICA PROFESIONAL DE VIDEO', N'PRPV', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (223, N'TEORIA ADMINISTRATIVA', N'TEAD', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (224, N'COMUNICACIÓN PUBLICA', N'COMP', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (225, N'PROTOCOLO II', N'PROT', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (226, N'TALLER DE PRENSA', N'TADP', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (227, N'CINEMATOGRAFIA', N'CINE', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (228, N'GESTION DE MERCADOS', N'GEME', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (229, N'PUBLICIDAD I', N'PUBL', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (230, N'COMUNICACIÓN INTERNACIONAL', N'COMI', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (231, N'PRACTICA PROFESIONAL DE MEDIOS IMPRESOS', N'PPMI', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (232, N'PRODUCCION CINEMATOGRAFICA', N'PRCE', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (233, N'PUBLICIDAD II', N'PUBL', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (234, N'RELACIONES INTERNACIONALES', N'REIN', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (235, N'….', N'AGNO', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (236, N'PSICOLOGIA DE LA EDUCACION Y DEL DESARROLLO', N'NULL', 4, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 6, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (237, N'TEORIA EDUCATIVA Y LEGISLACION', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (238, N'PEDAGOGIA GENERAL', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (239, N'ANATOMIA, SALUD Y ESTIMULACION TEMPRANA', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (240, N'EXPRESION ORAL Y ESCRITA I', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (241, N'CURRICULO Y GESTION', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (242, N'NEURODESARROLLO Y ESTIMULACION TEMPRANA', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (243, N'EDUCACION, COMUNIDAD Y FAMILIA', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (244, N'DIDACTICA DE LA PSICOMETRICIDAD Y LA EDUCACION FISICA', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (245, N'EXPRESION ORAL Y ESCRITA II', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (246, N'PSICOLOGIA DEL DESARROLLO INTEGRAL INFANTIL I(0 A 4 AÑOS)', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (247, N'DESARROLLO CURRICULAR DE LA EDUCACION INFANTIL DE 0 A 7 AÑOS', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (248, N'LAS TECNOLOGIAS DE LA INFORMACION Y LA COMUNICACIÓN APLICADAS A LA EDUCACION', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (249, N'EL JUEGO Y EL DESARROLLO INFANTIL', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (250, N'PRACTICA I: INTRODUCCION A LA INVESTIGACION EDUCATIVA', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (251, N'PSICOLOGIA DEL DESARROLLO INTEGRAL II (0 A 7 AÑOS)', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (252, N'DIDACTICA DE LA LENGUA EN LA EDUCACION INFANTIL DE 0 A 7 AÑOS', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (253, N'DIDACTICA PLASTICA EN LA EDUCACION INFANTIL DE 0 A 7 AÑOS', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (254, N'DIDACTICA DEL DESARROLLO DEL PENSAMIENTO LOGICO-MATEMATICO EN LA EDUCACION INFANTIL DE 0 A 7 AÑOS', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (255, N'PRACTICA II: DIAGNOSTICO EDUCATIVO Y APLICACIÓN DE CURRICULO', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (256, N'EXPRESION Y CREACION LITERARIA', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (257, N'DIDACTICA DE LA LENGUA EN LA EDUCACION INFANTIL DE 0 A 7 AÑOS', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (258, N'DIDACTICA DEL CONOCIMIENTO DEL MEDIO SOCIAL Y NEUTRAL EN LA EDUCACION INFANTIL', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (259, N'DIDACTICA DEL DESARROLLO DEL PENSAMIENTO LOGICO-MATEMATICO EN LA EDUCACION INFANTIL DE 0 A 7 AÑOS', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (260, N'PRACTICA III: PROYECTO SOCIO-ECONOMICO', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (261, N'ETICA Y MORAL', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (262, N'DIDACTICA DE LA EXPRESION MUNDIAL Y EXPRESION CORPORAL INFANTIL', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (263, N'DIDACTICA DEL CONOCIMINETO DEL MEDIO SOCIAL Y NATURAL EN LA EDUCACION INFANTIL DE 0 A 7 AÑOS', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (264, N'DESARROLLO PERSONAL, PROFESIONAL Y CULTURAL', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (265, N'PRACTICA IV: PROYECTO SOCIO-ECONOMICO', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (266, N'GESTION DE LA CALIDAD EDUCATIVA', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (267, N'PROGRAMAS Y PROYECTOS COMUNITARIOS DE ATENCION A LA NIÑEZ', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (268, N'ESTRATEGIAS DE EDUCACION INCLUSIVO', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (269, N'INVESTIGACOIN EDUCATIVA', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (270, N'EDUCACION INSTITUCIONAL Y DESEMPEÑO DOCENTE', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (271, N'PEDAGOGIA SOCIAL Y COMUNITARIA', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (272, N'GESTION INTERSECTORIAL', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (273, N'PRACTICA V: PROYECTO SOCIO-COMUNITARIO INTERSECTORIAL', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (274, N'PROCESOS DE FORMACION CENTRO EDUCATIVO, FAMILIAR Y COMUNIDAD', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (275, N'GESTION Y PREVENCION DE RIESGO', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (276, N'TALLER I', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (277, N'PRACTICA VI: PROYECTO SOCIO-COMUNITARIO INTERSECTORIAL', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (278, N'ORGANIZACIÓN Y ADMINISTRACION DE CENTROS EDUCATIVOS', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (279, N'TALLER II', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (280, N'TALLER III', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (281, N'PRACTICA VII: PROYECTO SOCIO-COMUNITARIO INTERSECTORIAL', N'NULL', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (282, N'INTRODUCCION AL DERECHO 1', N'INDE', 5, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 7, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (283, N'DERECHO ROMANO', N'DERO', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (284, N'GRAMATICA Y TECNICAS DE REDACCION', N'GYTR', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (285, N'INSTORUCCION AL ESTUDIO BIBLICO Y TEOLOGICO', N'IEBT', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (286, N'DERECHOS HUMANOS', N'DEHU', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (287, N'INTRODUCCION AL DERECHO II', N'INDEII', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 1)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (288, N'TEORIA DEL ESTADO', N'TEES', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (289, N'DERECHO AMBIENTAL', N'DEAM', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (290, N'ETICA Y DESARROLLO PROFESIONAL', N'ETDP', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (291, N'METODOS Y TECNICAS DE INVESTIGACION ', N'MYTI', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (292, N'DERECHO CIVIL I', N'DECI', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 2)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (293, N'DERECHO CONSTITUCIONAL I', N'DECO', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (294, N'INFORMATICA I', N'INFO', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (295, N'DERECHOS DE INTEGRACION', N'DEIN', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (296, N'INGLES I', N'INGLE', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (297, N'DERECHO CIVIL II', N'DECII', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 3)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (298, N'DERECHO CONSTITUCIONAL II', N'DECO', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (299, N'TECNICAS DE ORALIDAD ', N'TEOR', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 4)
GO
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (300, N'RPRINCIPIOS GERENCIALES DEL PROCESO', N'PGPR', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (301, N'INGLES II', N'INGLII', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (302, N'DERECHO CIVIL III', N'DECIIII', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 4)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (303, N'DERECHO PENAL I', N'DEPE', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (304, N'DERECHO MERCANTIL I', N'DEME', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (305, N'DERECHO DE CONSUMO', N'DCON', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (306, N'DERECHO LABORAL I', N'DELA', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (307, N'DERECHO CIVIL IV', N'DECI', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 5)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (308, N'DERECHO PENAL II', N'DEPE', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (309, N'DERECHO MERCANTIL II', N'DEMEII', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (310, N'DERECHO TRIBUTARIO', N'DERTR', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (311, N'DERECHO LABORAL II', N'DELA', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (312, N'DERECHO ADMINISTRATIVO I', N'DEAD', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 6)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (313, N'DERECHO PROCESAL PENAL I', N'DEPP', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (314, N'DERECHO PROCESAL CIVIL Y MERCANTIL I', N'DPCM', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (315, N'DERECHO DE FAMILIA', N'DEFA', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (316, N'DERECHO PROCESAL LABORAL', N'DEPL', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (317, N'DERECHO ADMINISTRATIVO II', N'DEADII', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 7)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (318, N'DERECHO PROCESAL PENAL II', N'DEPPII', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (319, N'DERECHO PRECESAL CIVIL Y MERCANTIL II', N'DECMII', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (320, N'DERECHO PROCESAL DE FAMILIA Y LEY CONTRA LA VIOLENCIA INTRAFAMILIAR', N'DPFV', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (321, N'DERECHO INTERNACIONAL PUBLICO', N'DEIP', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (322, N'CRIMINOLOGIA', N'CRIM', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 8)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (323, N'DERECHO Y PRACTICA REGISTRAL', N'DYPR', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (324, N'DERECHO PROCESAL CIVIL Y MERCANTIL III', N'DPCMIII', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (325, N'DERECHO DE LA PROPIEDAD INTELECTUAL', N'DEPI', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (326, N'DERECHO INTERNACIONAL PRIVADO', N'DIPR', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (327, N'DERECHO PENITENCIARIO', N'DPEN', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 9)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (328, N'MEDICINA FORENSE', N'MEFO', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (329, N'DERECHO PRACTICA NOTARIAL', N'DPNO', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (330, N'DERECHO BANCARIO', N'DEBA', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 10)
INSERT [dbo].[MATERIAS] ([materiaID], [nombre], [codigo], [carreraID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [pensumID], [cicloID]) VALUES (331, N'FILOSOFIA DEL DERECHO', N'FIDE', 7, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL, 8, 10)
SET IDENTITY_INSERT [dbo].[MATERIAS] OFF
SET IDENTITY_INSERT [dbo].[MUNICIPIO] ON 

INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (1, N'SN. CAYETANO IST.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (2, N'SANTA CLARA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (3, N'SANTO DOMINGO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (4, N'GUADALUPE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (5, N'NEPEAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (6, N'GUATEMALA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (7, N'QUEZALGUAQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (8, N'STOCKTON', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (9, N'SABANITAS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (10, N'CHOLUTECA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (11, N'TUXTA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (12, N'COMAYAHUELA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (13, N'LOS ANGELES', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (14, N'CINQUERA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 9)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (15, N'VILLA DOLORES', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 9)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (16, N'JUTIAPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 9)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (17, N'SAN ISIDRO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 9)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (18, N'SENSUNTEPEQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 9)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (19, N'VICTORIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 9)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (20, N'TEJUTEPEQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 9)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (21, N'ILOBASCO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 9)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (22, N'GUACOTECTI', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 9)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (23, N'EL ROSARIO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (24, N'SN. LUIS LA HERRA.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (25, N'ZACATECOLUCA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (26, N'TAPALHUACA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (27, N'SANTIAGO NONUALCO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (28, N'STA. MARIA OSTUMA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (29, N'SN. RAFAEL OBRAJ.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (30, N'SN. PEDRO NONUALCO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (31, N'SN. PEDRO MASAHUAT', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (32, N'SN. MIGUEL TEPEZ.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (33, N'SAN LUIS TALPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (34, N'SAN JUAN TEPEZONT.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (35, N'SAN JUAN TALPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (36, N'SN. JUAN NONUALCO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (37, N'CUYULTITAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (38, N'OLOCUILTA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (39, N'PARAISO DE OSOR.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (40, N'SN. ANT. MASAHUAT', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (41, N'SN. FCO. CHINAMECA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (42, N'SAN EMIGDIO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (43, N'MERC. LA CEIBA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (44, N'JERUSALEN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (45, N'CANDELARIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (46, N'COJUTEPEQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (47, N'PHOENIX', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 9)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (48, N'HYATTSVILLE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 8)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (49, N'BOSTON', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (50, N'NEW JERSEY', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (51, N'JAMAICA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (52, N'MARYLAND', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (53, N'NEW ORLEANS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (54, N'TAMAULIPAS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (55, N'SHERMAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (56, N'CONDADO BROOKS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (57, N'DALLAS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (58, N'VISTA HERMOSA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (59, N'ANAMOROS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (60, N'EL SAUCE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (61, N'YAYANTIQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (62, N'CONCEP. DE OTE.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (63, N'EL CARMEN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (64, N'LA UNION', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (65, N'SAN JOSE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (66, N'SAN ALEJO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (67, N'POLOROS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (68, N'PASAQUINA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (69, N'NUEVA ESPARTA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (70, N'LISLIQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (71, N'INTIPUCA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (72, N'CONCHAGUA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (73, N'BOLIVAR', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (74, N'YUCUAYQUIN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (75, N'MEANGUERA DEL G.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (76, N'STA. ROSA DE LIMA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 14)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (77, N'MEANGUERA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (78, N'SAN FERNANDO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (79, N'GUATAJIAGUA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (80, N'GUALOCOCTI', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (81, N'EL ROSARIO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (82, N'EL DIVISADERO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (83, N'DELIC. DE CONCEPCION', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (84, N'CHILANGA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (85, N'CORINTO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (86, N'CACAOPERA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (87, N'ARAMBALA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (88, N'YOLOAQUIN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (89, N'YAMABAL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (90, N'SOCIEDAD', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (91, N'SENSEMBRA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (92, N'SAN SIMON', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (93, N'SAN ISIDRO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (94, N'SAN FCO. GOTERA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (95, N'SAN CARLOS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (96, N'PERQUIN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (97, N'OSICALA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (98, N'LOLOTIQUILLO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (99, N'JOCORO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
GO
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (100, N'JOCOATIQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (101, N'JOATECA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (102, N'TOROLA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 13)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (103, N'CAROLINA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (104, N'CHAPELTIQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (105, N'CHINAMECA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (106, N'COMACARAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (107, N'CIUDAD BARRIOS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (108, N'CHIRILAGUA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (109, N'LOLOTIQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (110, N'ULUAZAPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (111, N'SESORI', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (112, N'SAN RAFAEL OTE.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (113, N'SAN MIGUEL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (114, N'SAN LUIS DE LA REINA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (115, N'SAN JORGE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (116, N'SAN GERARDO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (117, N'SN. ANT. DE MOSCO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (118, N'QUELEPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (119, N'NVO. EDEN DE S. J.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (120, N'NVA. GUADALUPE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (121, N'MONCAGUA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (122, N'EL TRANSITO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 12)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (123, N'SANTA MARIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (124, N'SANTIAGO DE MARIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (125, N'USULUTAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (126, N'BERLIN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (127, N'CONC. BATRES', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (128, N'EREGUYQUIN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (129, N'JIQUILISCO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (130, N'VAN BURE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (131, N'SANTA ELENA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (132, N'SAN DIONISIO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (133, N'SN. BUENAVENTURA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (134, N'SAN AGUSTIN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (135, N'PTO. EL TRIUNFO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (136, N'OZATLAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (137, N'NUEVA GRANADA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (138, N'MERCEDES UMA?A', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (139, N'JUCUARAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (140, N'JUCUAPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (141, N'ESTANZUELAS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (142, N'EL TRIUNFO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (143, N'CALIFORNIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (144, N'ALEGRIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (145, N'SAN FCO. JAVIER', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (146, N'TECAPAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 11)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (147, N'PENNSYLVANIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (148, N'SAN ILDEFONSO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (149, N'VERAPAZ', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (150, N'TEPETITAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (151, N'TECOLUCA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (152, N'SAN VICENTE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (153, N'SAN SEBASTIAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (154, N'SAN LORENZO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (155, N'SN. EST. CATARINA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (156, N'APASTEPEQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 10)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (157, N'EL ROSARIO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (158, N'SAN B. PERULAPIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (159, N'ORAT. DECONCEP.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (160, N'MONTE SAN JUAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (161, N'EL CARMEN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (162, N'SAN CRISTOBAL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (163, N'TENANCINGO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (164, N'SUCHITOTO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (165, N'SANTA C. MICHAPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (166, N'SANTA C. ANALQUITO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (167, N'SAN RAMON', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (168, N'SAN RAFAEL CEDROS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (169, N'SAN P. PERULAPAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (170, N'SAN J. GUAYABAL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 7)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (171, N'AGUILARES', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (172, N'APOPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (173, N'CUSCATANCINGO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (174, N'ILOPANGO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (175, N'GUAZAPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (176, N'EL PAISNAL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (177, N'AYUTUXTEPEQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (178, N'MEJICANOS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (179, N'CIUDAD DELGADO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (180, N'TONACATEPEQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (181, N'SOYAPANGO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (182, N'SANTO TOMAS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (183, N'STGO. TEXACUANGOS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (184, N'SAN SALVADOR', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (185, N'SAN MARTIN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (186, N'SAN MARCOS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (187, N'ROSARIO DE MORA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (188, N'PANCHIMALCO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (189, N'NEJAPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 6)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (190, N'ANTIGUO CUSCATLAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (191, N'COMASAGUA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (192, N'CHILTIUPAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (193, N'JAYAQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (194, N'LA LIBERTAD', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (195, N'NVA. SAN SALVADOR', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (196, N'SACACOYO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (197, N'SAN JUAN OPICO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (198, N'ZARAGOZA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (199, N'TEPECOYO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
GO
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (200, N'TEOTEPEQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (201, N'TALNIQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (202, N'TAMANIQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (203, N'SAN P. TACACHICO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (204, N'SAN MATIAS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (205, N'SAN J. VILLANUEVA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (206, N'QUEZALTEPEQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (207, N'NVO. CUSCATLAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (208, N'JICALAPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (209, N'HUIZUCAR', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (210, N'CIUDAD ARCE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (211, N'COLON', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 5)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (212, N'COMALAPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (213, N'S. JOSE LAS FLORES', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (214, N'CONCEP. QUEZALT.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (215, N'CHALATENANGO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (216, N'DULCE N. DE MARIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (217, N'EL CARRIZAL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (218, N'EL PARAISO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (219, N'LA LAGUNA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (220, N'LA PALMA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (221, N'LA REINA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (222, N'CITALA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (223, N'AZACUALPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (224, N'ARCATAO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (225, N'AGUA CALIENTE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (226, N'TEJUTLA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (227, N'SANTA RITA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (228, N'SAN RAFAEL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (229, N'S. MIGUEL DE MERC.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (230, N'SAN LUIS DEL CARMEN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (231, N'LAS VUELTAS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (232, N'NOMBRE DE JESUS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (233, N'NVA. CONCEPCION', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (234, N'NVA. TRINIDAD', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (235, N'OJOS DE AGUA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (236, N'POTONICO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (237, N'SAN ANT. LA CRUZ', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (238, N'SAN ANT. LOS RANCHOS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (239, N'SAN FERNANDO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (240, N'SAN FCO. LEMPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (241, N'SAN FCO. MORAZAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (242, N'SAN IGNACIO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (243, N'S. ISIDRO LABRA.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (244, N'SAN J. CANCASQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 4)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (245, N'ACAJUTLA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (246, N'NAHUIZALCO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (247, N'SONZACATE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (248, N'SONSONATE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (249, N'STO. DOM. DE GUZMAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (250, N'STA. CAT. MASAHUAT', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (251, N'SAN JULIAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (252, N'SAN ANT. DEL MONTE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (253, N'SALCOATITAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (254, N'NAHULINGO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (255, N'JUAYUA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (256, N'ARMENIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (257, N'CUISNAHUAT', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (258, N'STA. ISABEL ISHUAT', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (259, N'IZALCO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (260, N'CALUCO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 3)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (261, N'SANTA ANA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (262, N'STA. ROSA GUACHIP.', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (263, N'SAN SEB. SALITRILLO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (264, N'S. ANT. PAJONAL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (265, N'METAPAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (266, N'MASAHUAT', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (267, N'EL PORVENIR', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (268, N'EL CONGO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (269, N'CHALCHUAPA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (270, N'COATEPEQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (271, N'CAND. DE LA FRONTERA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (272, N'STGO. DE LA FRONTERA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (273, N'TEXISTEPEQUE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 2)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (274, N'LA HABANA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (275, N'CONCEPCION DE ATACO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (276, N'GUAYMANGO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (277, N'SAN FCO. MENEDEZ', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (278, N'SAN PEDRO PUXTLA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (279, N'TURIN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (280, N'AHUACHAPAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (281, N'TACUBA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (282, N'SAN LORENZO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (283, N'JUJUTLA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (284, N'EL REFUGIO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (285, N'ATIQUIZAYA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[MUNICIPIO] ([municipioID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [departamentoID]) VALUES (286, N'APANECA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[MUNICIPIO] OFF
SET IDENTITY_INSERT [dbo].[NOTAS] ON 

INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (1, 1, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (2, 2, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (3, 3, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (4, 4, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (5, 5, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (6, 6, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (7, 7, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (8, 8, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (9, 9, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (10, 10, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (11, 11, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (12, 12, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (13, 13, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (14, 14, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (15, 15, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (16, 16, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (17, 17, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (18, 18, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (19, 19, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (20, 20, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (21, 21, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (22, 22, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (23, 23, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (24, 24, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (25, 25, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (26, 26, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (27, 27, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (28, 28, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (29, 29, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (30, 31, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (31, 32, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (32, 33, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (33, 34, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (34, 35, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (35, 36, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (36, 37, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (37, 38, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (38, 39, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (39, 40, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (40, 43, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (41, 44, CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(8.50 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
INSERT [dbo].[NOTAS] ([notaID], [idMateria], [notaUno], [notaDos], [notaTres], [notaCuatro], [notaCinco], [notaSeis], [notaSiete], [notaOcho], [notaNueve], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [alumnoID]) VALUES (42, 11, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[NOTAS] OFF
SET IDENTITY_INSERT [dbo].[PAIS] ON 

INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (1, N'ALEMANIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'ALEMAN')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (2, N'ARGENTINA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'ARGENTINO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (3, N'AUSTRALIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'AUSTRALIANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (4, N'AUSTRIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'AUSTRIA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (5, N'BANGLADESH', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'BANGLADESH')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (6, N'BELGICA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'BELGA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (7, N'BELIZE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'BELICE?O')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (8, N'BOLIVIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'BOLIVIANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (9, N'BRAZIL', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'BRASILE?O')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (10, N'BULGARIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'BULGARIA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (11, N'CAMBOYA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'CAMBOYA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (12, N'CANADA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'CANADIENSE')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (13, N'CAYMAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'CAYMANES')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (14, N'CHILE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'CHILENO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (15, N'COLOMBIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'COLOMBIANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (16, N'COREA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'COREANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (17, N'COREA DEL SUR (REP DE COREA)', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'COREA DEL SUR (REP DE COREA)')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (18, N'COSTA RICA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'COSTARRICENSE')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (19, N'CUBA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'CUBANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (20, N'ECUADOR', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'ECUATORIANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (21, N'EL SALVADOR', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'SALVADORE?O')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (22, N'ESPA?A', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'ESPA?OL')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (23, N'ESTADOS UNIDOS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'ESTADOUNIDENSE')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (24, N'FILIPINAS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'FILIPINO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (25, N'FINLANDIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'FINLANDESA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (26, N'FRANCIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'FRANCESA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (27, N'GUATEMALA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'GUATEMALTECO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (28, N'HAITI', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'HAITIANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (29, N'HINDU', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'HINDU')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (30, N'HOLANDA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'HOLANDES')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (31, N'HONDURAS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'HONDURE?O')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (32, N'INGLATERRA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'BRITANICA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (33, N'IRAK', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'IRAQUI')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (34, N'IRAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'IRANI')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (35, N'IRLANDA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'IRLANDES')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (36, N'ITALIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'ITALIANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (37, N'JAPON', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'JAPONES')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (38, N'MALAYSIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'MALAYSIA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (39, N'MEXICO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'MEXICANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (40, N'NEOZELANDESA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'NEOZELANDESA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (41, N'NICARAGUA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'NICARAGUENSE')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (42, N'NIGERIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'NIGERIA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (43, N'NO PALICA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'NO APLICA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (44, N'NORUEGA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'NORUEGA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (45, N'PANAMA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'PANAME?O')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (46, N'PARAGUAY', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'PARAGUAYO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (47, N'PERU', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'PERUANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (48, N'POLONIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'POLACO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (49, N'PORTUGUESA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'PORTUGUESA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (50, N'PUERTO RICO', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'PUERTORRIQUE?O')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (51, N'REPUBLICA CHECA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'REPUBLICA CHECA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (52, N'REPUBLICA DE CHINA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'CHINO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (53, N'REPUBLICA DE SINGAPORE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'REPUBLICA DE SINGAPORE')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (54, N'REPUBLICA DE TOGOLAISE', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'REPUBLICA DE TOGOLAISE')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (55, N'REPUBLICA DOMINICANA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'DOMINICANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (56, N'RUMANIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'RUMANA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (57, N'RUSA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'RUSA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (58, N'SAUDI ARABIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'SAUDI ARABIA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (59, N'SRI LANKAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'SRI LANKAN')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (60, N'SUECIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'SUECIA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (61, N'SUIZA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'SUIZA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (62, N'SUR AFRICA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'SUR AFRICA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (63, N'TAILAN PROVINCIA DE CHINA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'TAILAN PROVINCIA DE CHINA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (64, N'TERRITORIOS PALESTINOS', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'TERRITORIOS PALESTINOS')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (65, N'UCRANIA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'UCRANIA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (66, N'UGANDA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'UGANDA')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (67, N'URUGUAY', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'URUGUAYO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (68, N'UZBEKISTAN', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'UZBEKISTAN')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (69, N'VENEZUELA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'VENEZOLANO')
INSERT [dbo].[PAIS] ([paisID], [nombre], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [nacionalidad]) VALUES (70, N'YUGOSLAVA', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'YUGOSLAVA')
SET IDENTITY_INSERT [dbo].[PAIS] OFF
SET IDENTITY_INSERT [dbo].[PENSUM] ON 

INSERT [dbo].[PENSUM] ([pensumID], [carreraID], [nombre], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, 6, N'Plan de estudios año 2010', 2010, 1, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[PENSUM] ([pensumID], [carreraID], [nombre], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, 1, N'Plan de estudios año 2015', 2015, 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[PENSUM] ([pensumID], [carreraID], [nombre], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (4, 2, N'Plan de estudios año 2015', 2015, 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[PENSUM] ([pensumID], [carreraID], [nombre], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (5, 3, N'Plan de estudios año 2015', 2015, 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[PENSUM] ([pensumID], [carreraID], [nombre], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (6, 4, N'Plan de estudios año 2017-2021', 2017, 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[PENSUM] ([pensumID], [carreraID], [nombre], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (7, 5, N'Plan de estudios año 2013', 2013, 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[PENSUM] ([pensumID], [carreraID], [nombre], [anio], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (8, 7, N'Plan de estudios año 2015', 2015, 1, CAST(N'2020-05-23' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[PENSUM] OFF
SET IDENTITY_INSERT [dbo].[TIPO_CARNET] ON 

INSERT [dbo].[TIPO_CARNET] ([tipoCarnetId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, N'Carnet Administrativo', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[TIPO_CARNET] ([tipoCarnetId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (2, N'Carnet Docente', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[TIPO_CARNET] ([tipoCarnetId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, N'Carnet Alumno', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[TIPO_CARNET] OFF
SET IDENTITY_INSERT [dbo].[TIPO_DOCUMENTOS] ON 

INSERT [dbo].[TIPO_DOCUMENTOS] ([documentoId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, N'Solicitud de Ingreso', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[TIPO_DOCUMENTOS] ([documentoId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (2, N'Fotografías', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[TIPO_DOCUMENTOS] ([documentoId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, N'Partida de Nacimiento', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[TIPO_DOCUMENTOS] ([documentoId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (4, N'DUI', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[TIPO_DOCUMENTOS] ([documentoId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (5, N'NIT', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[TIPO_DOCUMENTOS] ([documentoId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (6, N'Titulo de Bachiller', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[TIPO_DOCUMENTOS] ([documentoId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (7, N'Nota PAES', CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[TIPO_DOCUMENTOS] OFF
SET IDENTITY_INSERT [dbo].[TIPO_INGRESO] ON 

INSERT [dbo].[TIPO_INGRESO] ([tipoIngresoId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codTipIngreso]) VALUES (1, N'Nuevo Ingreso', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'NIN')
INSERT [dbo].[TIPO_INGRESO] ([tipoIngresoId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codTipIngreso]) VALUES (2, N'Equivalencias', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'EQV')
INSERT [dbo].[TIPO_INGRESO] ([tipoIngresoId], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codTipIngreso]) VALUES (3, N'Equivalencias Extrajero', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'EQE')
SET IDENTITY_INSERT [dbo].[TIPO_INGRESO] OFF
SET IDENTITY_INSERT [dbo].[TIPO_USUARIO] ON 

INSERT [dbo].[TIPO_USUARIO] ([tipoUsrID], [nombreTipo], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codTipUsr]) VALUES (1, N'Administrador sistema', N'Tipos de usuario con acceso a modificaciones mayores', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'admin')
INSERT [dbo].[TIPO_USUARIO] ([tipoUsrID], [nombreTipo], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codTipUsr]) VALUES (2, N'Personal administrativo', N'Tipos de usuario con acceso a ciertos modulos del sistema', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'padmi')
INSERT [dbo].[TIPO_USUARIO] ([tipoUsrID], [nombreTipo], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codTipUsr]) VALUES (3, N'Personal Docente', N'Tipos de usuario con acceso a ciertos modulos de catedraticos', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'docen')
INSERT [dbo].[TIPO_USUARIO] ([tipoUsrID], [nombreTipo], [descripcion], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor], [codTipUsr]) VALUES (4, N'Personal Alumnado', N'Tipos de usuario con acceso a modulos de alumnos', CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL, N'alumn')
SET IDENTITY_INSERT [dbo].[TIPO_USUARIO] OFF
SET IDENTITY_INSERT [dbo].[UNIVERSIDAD] ON 

INSERT [dbo].[UNIVERSIDAD] ([universidadID], [nombre], [direccion], [telefono], [campusID], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, N'Universidad Cristiana de las Asambleas de Dios', N'27 calle Oriente N134, barrio San Miguelito (entre reatro de Cámara y La Cascada), San Salvador, El Salvador C.A.', N'2520-7500', 1, CAST(N'2020-05-22' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[UNIVERSIDAD] OFF
SET IDENTITY_INSERT [dbo].[USUARIOS] ON 

INSERT [dbo].[USUARIOS] ([usuarioID], [nombreUSR], [contrasena], [tipoUsrID], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (1, N'sys', N'sys', 1, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[USUARIOS] ([usuarioID], [nombreUSR], [contrasena], [tipoUsrID], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (2, N'admin', N'admin', 2, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[USUARIOS] ([usuarioID], [nombreUSR], [contrasena], [tipoUsrID], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (3, N'docen', N'docen', 3, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
INSERT [dbo].[USUARIOS] ([usuarioID], [nombreUSR], [contrasena], [tipoUsrID], [activo], [fechaAdicion], [adicionadoPor], [fechaModificacion], [modificadoPor]) VALUES (4, N'balfaro', N'balfaro', 4, 1, CAST(N'2020-05-26' AS Date), N'sys', NULL, NULL)
SET IDENTITY_INSERT [dbo].[USUARIOS] OFF
ALTER TABLE [dbo].[ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_ALUMNO_CAMPUS] FOREIGN KEY([campusID])
REFERENCES [dbo].[CAMPUS] ([campusID])
GO
ALTER TABLE [dbo].[ALUMNO] CHECK CONSTRAINT [FK_ALUMNO_CAMPUS]
GO
ALTER TABLE [dbo].[ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_ALUMNO_CARRERAS] FOREIGN KEY([carreraID])
REFERENCES [dbo].[CARRERAS] ([carreraID])
GO
ALTER TABLE [dbo].[ALUMNO] CHECK CONSTRAINT [FK_ALUMNO_CARRERAS]
GO
ALTER TABLE [dbo].[ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_ALUMNO_DEPARTAMENTO] FOREIGN KEY([departamentoID])
REFERENCES [dbo].[DEPARTAMENTO] ([departamentoID])
GO
ALTER TABLE [dbo].[ALUMNO] CHECK CONSTRAINT [FK_ALUMNO_DEPARTAMENTO]
GO
ALTER TABLE [dbo].[ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_ALUMNO_ESTADO_CIVIL] FOREIGN KEY([estadoCivilID])
REFERENCES [dbo].[ESTADO_CIVIL] ([estadoCivilID])
GO
ALTER TABLE [dbo].[ALUMNO] CHECK CONSTRAINT [FK_ALUMNO_ESTADO_CIVIL]
GO
ALTER TABLE [dbo].[ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_ALUMNO_MUNICIPIO] FOREIGN KEY([municipioID])
REFERENCES [dbo].[MUNICIPIO] ([municipioID])
GO
ALTER TABLE [dbo].[ALUMNO] CHECK CONSTRAINT [FK_ALUMNO_MUNICIPIO]
GO
ALTER TABLE [dbo].[ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_ALUMNO_PAIS] FOREIGN KEY([paisID])
REFERENCES [dbo].[PAIS] ([paisID])
GO
ALTER TABLE [dbo].[ALUMNO] CHECK CONSTRAINT [FK_ALUMNO_PAIS]
GO
ALTER TABLE [dbo].[ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_ALUMNO_TIPO_INGRESO] FOREIGN KEY([tipoIngresoId])
REFERENCES [dbo].[TIPO_INGRESO] ([tipoIngresoId])
GO
ALTER TABLE [dbo].[ALUMNO] CHECK CONSTRAINT [FK_ALUMNO_TIPO_INGRESO]
GO
ALTER TABLE [dbo].[ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_ALUMNO_TIPO_USUARIO] FOREIGN KEY([tipoUsrID])
REFERENCES [dbo].[TIPO_USUARIO] ([tipoUsrID])
GO
ALTER TABLE [dbo].[ALUMNO] CHECK CONSTRAINT [FK_ALUMNO_TIPO_USUARIO]
GO
ALTER TABLE [dbo].[CARNET]  WITH CHECK ADD  CONSTRAINT [FK_CARNET_ALUMNO] FOREIGN KEY([alumnoID])
REFERENCES [dbo].[ALUMNO] ([alumnoID])
GO
ALTER TABLE [dbo].[CARNET] CHECK CONSTRAINT [FK_CARNET_ALUMNO]
GO
ALTER TABLE [dbo].[CARNET]  WITH CHECK ADD  CONSTRAINT [FK_CARNET_TIPO_CARNET] FOREIGN KEY([tipoCarnetId])
REFERENCES [dbo].[TIPO_CARNET] ([tipoCarnetId])
GO
ALTER TABLE [dbo].[CARNET] CHECK CONSTRAINT [FK_CARNET_TIPO_CARNET]
GO
ALTER TABLE [dbo].[CARRERAS]  WITH CHECK ADD  CONSTRAINT [FK_CARRERAS_FACULTADES] FOREIGN KEY([facultadesID])
REFERENCES [dbo].[FACULTADES] ([facultadesID])
GO
ALTER TABLE [dbo].[CARRERAS] CHECK CONSTRAINT [FK_CARRERAS_FACULTADES]
GO
ALTER TABLE [dbo].[CATEDRATICOS]  WITH CHECK ADD  CONSTRAINT [FK_CATEDRATICOS_DEPARTAMENTO] FOREIGN KEY([departamentoID])
REFERENCES [dbo].[DEPARTAMENTO] ([departamentoID])
GO
ALTER TABLE [dbo].[CATEDRATICOS] CHECK CONSTRAINT [FK_CATEDRATICOS_DEPARTAMENTO]
GO
ALTER TABLE [dbo].[CATEDRATICOS]  WITH CHECK ADD  CONSTRAINT [FK_CATEDRATICOS_ESTADO_CIVIL] FOREIGN KEY([estadoCivilID])
REFERENCES [dbo].[ESTADO_CIVIL] ([estadoCivilID])
GO
ALTER TABLE [dbo].[CATEDRATICOS] CHECK CONSTRAINT [FK_CATEDRATICOS_ESTADO_CIVIL]
GO
ALTER TABLE [dbo].[CATEDRATICOS]  WITH CHECK ADD  CONSTRAINT [FK_CATEDRATICOS_PAIS] FOREIGN KEY([paisID])
REFERENCES [dbo].[PAIS] ([paisID])
GO
ALTER TABLE [dbo].[CATEDRATICOS] CHECK CONSTRAINT [FK_CATEDRATICOS_PAIS]
GO
ALTER TABLE [dbo].[CATEDRATICOS]  WITH CHECK ADD  CONSTRAINT [FK_CATEDRATICOS_TIPO_USUARIO] FOREIGN KEY([tipoUsrID])
REFERENCES [dbo].[TIPO_USUARIO] ([tipoUsrID])
GO
ALTER TABLE [dbo].[CATEDRATICOS] CHECK CONSTRAINT [FK_CATEDRATICOS_TIPO_USUARIO]
GO
ALTER TABLE [dbo].[DEPARTAMENTO]  WITH CHECK ADD  CONSTRAINT [FK_DEPARTAMENTO_PAIS] FOREIGN KEY([paisId])
REFERENCES [dbo].[PAIS] ([paisID])
GO
ALTER TABLE [dbo].[DEPARTAMENTO] CHECK CONSTRAINT [FK_DEPARTAMENTO_PAIS]
GO
ALTER TABLE [dbo].[DOCUM_X_ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_DOCUM_X_ALUMNO_ALUMNO] FOREIGN KEY([alumnoID])
REFERENCES [dbo].[ALUMNO] ([alumnoID])
GO
ALTER TABLE [dbo].[DOCUM_X_ALUMNO] CHECK CONSTRAINT [FK_DOCUM_X_ALUMNO_ALUMNO]
GO
ALTER TABLE [dbo].[DOCUM_X_ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_DOCUM_X_ALUMNO_TIPO_DOCUMENTOS] FOREIGN KEY([documentoId])
REFERENCES [dbo].[TIPO_DOCUMENTOS] ([documentoId])
GO
ALTER TABLE [dbo].[DOCUM_X_ALUMNO] CHECK CONSTRAINT [FK_DOCUM_X_ALUMNO_TIPO_DOCUMENTOS]
GO
ALTER TABLE [dbo].[FACULTADES]  WITH CHECK ADD  CONSTRAINT [FK_FACULTADES_UNIVERSIDAD] FOREIGN KEY([universidadID])
REFERENCES [dbo].[UNIVERSIDAD] ([universidadID])
GO
ALTER TABLE [dbo].[FACULTADES] CHECK CONSTRAINT [FK_FACULTADES_UNIVERSIDAD]
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_ALUMNO] FOREIGN KEY([alumnoID])
REFERENCES [dbo].[ALUMNO] ([alumnoID])
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO] CHECK CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_ALUMNO]
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_ESTADO_MATERIA] FOREIGN KEY([estaMateriaID])
REFERENCES [dbo].[ESTADO_MATERIA] ([estaMateriaID])
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO] CHECK CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_ESTADO_MATERIA]
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_GRUPOS] FOREIGN KEY([grupoID])
REFERENCES [dbo].[GRUPOS] ([grupoID])
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO] CHECK CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_GRUPOS]
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO]  WITH CHECK ADD  CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_MATERIAS] FOREIGN KEY([materiaID])
REFERENCES [dbo].[MATERIAS] ([materiaID])
GO
ALTER TABLE [dbo].[INSCRIPCION_X_ALUMNO] CHECK CONSTRAINT [FK_INSCRIPCION_X_ALUMNO_MATERIAS]
GO
ALTER TABLE [dbo].[MATERIAS]  WITH CHECK ADD  CONSTRAINT [FK_MATERIAS_CARRERAS] FOREIGN KEY([carreraID])
REFERENCES [dbo].[CARRERAS] ([carreraID])
GO
ALTER TABLE [dbo].[MATERIAS] CHECK CONSTRAINT [FK_MATERIAS_CARRERAS]
GO
ALTER TABLE [dbo].[MATERIAS]  WITH CHECK ADD  CONSTRAINT [FK_MATERIAS_CICLOS] FOREIGN KEY([cicloID])
REFERENCES [dbo].[CICLOS] ([cicloID])
GO
ALTER TABLE [dbo].[MATERIAS] CHECK CONSTRAINT [FK_MATERIAS_CICLOS]
GO
ALTER TABLE [dbo].[MATERIAS]  WITH CHECK ADD  CONSTRAINT [FK_MATERIAS_PENSUM] FOREIGN KEY([pensumID])
REFERENCES [dbo].[PENSUM] ([pensumID])
GO
ALTER TABLE [dbo].[MATERIAS] CHECK CONSTRAINT [FK_MATERIAS_PENSUM]
GO
ALTER TABLE [dbo].[MUNICIPIO]  WITH CHECK ADD  CONSTRAINT [FK_MUNICIPIO_DEPARTAMENTO] FOREIGN KEY([departamentoID])
REFERENCES [dbo].[DEPARTAMENTO] ([departamentoID])
GO
ALTER TABLE [dbo].[MUNICIPIO] CHECK CONSTRAINT [FK_MUNICIPIO_DEPARTAMENTO]
GO
ALTER TABLE [dbo].[NOTAS]  WITH CHECK ADD  CONSTRAINT [FK_NOTAS_ALUMNO] FOREIGN KEY([alumnoID])
REFERENCES [dbo].[ALUMNO] ([alumnoID])
GO
ALTER TABLE [dbo].[NOTAS] CHECK CONSTRAINT [FK_NOTAS_ALUMNO]
GO
ALTER TABLE [dbo].[NOTAS]  WITH CHECK ADD  CONSTRAINT [FK_NOTAS_MATERIAS] FOREIGN KEY([idMateria])
REFERENCES [dbo].[MATERIAS] ([materiaID])
GO
ALTER TABLE [dbo].[NOTAS] CHECK CONSTRAINT [FK_NOTAS_MATERIAS]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO]  WITH CHECK ADD  CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_DEPARTAMENTO] FOREIGN KEY([departamentoID])
REFERENCES [dbo].[DEPARTAMENTO] ([departamentoID])
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] CHECK CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_DEPARTAMENTO]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO]  WITH CHECK ADD  CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_ESTADO_CIVIL] FOREIGN KEY([estadoCivilID])
REFERENCES [dbo].[ESTADO_CIVIL] ([estadoCivilID])
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] CHECK CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_ESTADO_CIVIL]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO]  WITH CHECK ADD  CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_MUNICIPIO] FOREIGN KEY([municipioID])
REFERENCES [dbo].[MUNICIPIO] ([municipioID])
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] CHECK CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_MUNICIPIO]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO]  WITH CHECK ADD  CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_PAIS] FOREIGN KEY([paisID])
REFERENCES [dbo].[PAIS] ([paisID])
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] CHECK CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_PAIS]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO]  WITH CHECK ADD  CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_PUESTO_ADMINISTRATIVO] FOREIGN KEY([puestoID])
REFERENCES [dbo].[PUESTO_ADMINISTRATIVO] ([puestoID])
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] CHECK CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_PUESTO_ADMINISTRATIVO]
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO]  WITH CHECK ADD  CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_TIPO_USUARIO] FOREIGN KEY([tipoUsrID])
REFERENCES [dbo].[TIPO_USUARIO] ([tipoUsrID])
GO
ALTER TABLE [dbo].[PERSONAL_ADMINISTRATIVO] CHECK CONSTRAINT [FK_PERSONAL_ADMINISTRATIVO_TIPO_USUARIO]
GO
ALTER TABLE [dbo].[UNIVERSIDAD]  WITH CHECK ADD  CONSTRAINT [FK_UNIVERSIDAD_CAMPUS] FOREIGN KEY([campusID])
REFERENCES [dbo].[CAMPUS] ([campusID])
GO
ALTER TABLE [dbo].[UNIVERSIDAD] CHECK CONSTRAINT [FK_UNIVERSIDAD_CAMPUS]
GO
ALTER TABLE [dbo].[USUARIOS]  WITH CHECK ADD  CONSTRAINT [FK_USUARIOS_TIPO_USUARIO] FOREIGN KEY([tipoUsrID])
REFERENCES [dbo].[TIPO_USUARIO] ([tipoUsrID])
GO
ALTER TABLE [dbo].[USUARIOS] CHECK CONSTRAINT [FK_USUARIOS_TIPO_USUARIO]
GO
/****** Object:  Statistic [_WA_Sys_00000018_2D27B809]    Script Date: 26/05/2020 16:24:16 ******/
CREATE STATISTICS [_WA_Sys_00000018_2D27B809] ON [dbo].[ALUMNO]([carreraID]) WITH STATS_STREAM = 0x01000000010000000000000000000000D8495C9E00000000CB010000000000008B01000000000000380200003800000004000A000000000000000000000000000700000049FEEB00C7AB000001000000000000000100000000000000000000000000803F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000010000000100000014000000000080400000803F00000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000001F0000000000000027000000000000000800000000000000100014000000803F000000000000803F060000000400000100000000000000
GO
/****** Object:  Statistic [PK__ALUMNO__375A03E45D6D3216]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[ALUMNO]([PK__ALUMNO__375A03E45D6D3216]) WITH STATS_STREAM = 0x0100000001000000000000000000000083F9633E00000000CB010000000000008B01000000000000380300003800000004000A00000000000000000000000000070000009D48F400C7AB000001000000000000000100000000000000000000000000803F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000010000000100000014000000000080400000803F00000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000001F0000000000000027000000000000000800000000000000100014000000803F000000000000803F010000000400000100000000000000, ROWCOUNT = 1, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK_ANIO_ELECTIVO]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[ANIO_ELECTIVO]([PK_ANIO_ELECTIVO]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 38, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__CAMPUS__D03AB2C02D57E8B7]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[CAMPUS]([PK__CAMPUS__D03AB2C02D57E8B7]) WITH STATS_STREAM = 0x01000000010000000000000000000000A3D3C7DB0000000040000000000000000000000000000000380300003800000004000A00000000000000000038000000, ROWCOUNT = 1, PAGECOUNT = 1
GO
/****** Object:  Statistic [_WA_Sys_00000002_282DF8C2]    Script Date: 26/05/2020 16:24:16 ******/
CREATE STATISTICS [_WA_Sys_00000002_282DF8C2] ON [dbo].[CARNET]([alumnoID]) WITH STATS_STREAM = 0x010000000100000000000000000000000E1942AF00000000CB010000000000008B01000000000000380300003800000004000A000000000000000000010000000700000055CE0D01C7AB000001000000000000000100000000000000000000000000803F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000010000000100000014000000000080400000803F00000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000001F0000000000000027000000000000000800000000000000100014000000803F000000000000803F010000000400000100000000000000
GO
/****** Object:  Statistic [_WA_Sys_00000003_282DF8C2]    Script Date: 26/05/2020 16:24:16 ******/
CREATE STATISTICS [_WA_Sys_00000003_282DF8C2] ON [dbo].[CARNET]([tipoCarnetId]) WITH STATS_STREAM = 0x010000000100000000000000000000000D297BAD00000000CB010000000000008B01000000000000380300003800000004000A00000000000000000001000000070000004ECE0D01C7AB000001000000000000000100000000000000000000000000803F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000010000000100000014000000000080400000803F00000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000001F0000000000000027000000000000000800000000000000100014000000803F000000000000803F030000000400000100000000000000
GO
/****** Object:  Statistic [PK_CARNET]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[CARNET]([PK_CARNET]) WITH STATS_STREAM = 0x010000000100000000000000000000003E3DC34B00000000CB010000000000008B01000000000000380300003800000004000A00000000000000000000000000070000002ACE0D01C7AB000001000000000000000100000000000000000000000000803F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000010000000100000014000000000080400000803F00000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000001F0000000000000027000000000000000800000000000000100014000000803F000000000000803F030000000400000100000000000000, ROWCOUNT = 1, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__CARRERAS__C837724BCB497011]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[CARRERAS]([PK__CARRERAS__C837724BCB497011]) WITH STATS_STREAM = 0x01000000010000000000000000000000B185EED0000000002802000000000000E801000000000000380300003800000004000A00000000000000000000000000070000008665AA00C4AB0000070000000000000007000000000000000000803F2549123E000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004000000040000000100000014000000000080400000E04000000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000007C000000000000008400000000000000200000000000000037000000000000004E000000000000006500000000000000100014000000803F000000000000803F01000000040000100014000000803F0000803F0000803F03000000040000100014000000803F0000803F0000803F05000000040000100014000000803F0000803F0000803F070000000400000700000000000000, ROWCOUNT = 7, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__CATEDRAT__3AC9AF7BDC717AE3]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[CATEDRATICOS]([PK__CATEDRAT__3AC9AF7BDC717AE3]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 0, PAGECOUNT = 0
GO
/****** Object:  Statistic [PK__CICLOS__D452528D57C00DBF]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[CICLOS]([PK__CICLOS__D452528D57C00DBF]) WITH STATS_STREAM = 0x01000000010000000000000000000000479AE3DC0000000066020000000000002602000000000000380300003800000004000A00000000000000000000000000070000008965AA00C4AB00000A000000000000000A000000000000000000803FCDCCCC3D00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000600000006000000010000001400000000008040000020410000000000008040000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011000000000000000000000000000000BA00000000000000C200000000000000300000000000000047000000000000005E0000000000000075000000000000008C00000000000000A300000000000000100014000000803F000000000000803F01000000040000100014000000803F0000803F0000803F03000000040000100014000000803F0000803F0000803F05000000040000100014000000803F0000803F0000803F07000000040000100014000000803F0000803F0000803F09000000040000100014000000803F000000000000803F0A0000000400000A00000000000000, ROWCOUNT = 10, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__DEPARTAM__A67AC1588A3DA21F]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[DEPARTAMENTO]([PK__DEPARTAM__A67AC1588A3DA21F]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 14, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK_DOCUM_X_ALUMNO]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[DOCUM_X_ALUMNO]([PK_DOCUM_X_ALUMNO]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 7, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__ESTADO_C__FF92122F3820E729]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[ESTADO_CIVIL]([PK__ESTADO_C__FF92122F3820E729]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 5, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK_ESTADO_MATERIA]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[ESTADO_MATERIA]([PK_ESTADO_MATERIA]) WITH STATS_STREAM = 0x01000000010000000000000000000000FADA63320000000066020000000000002602000000000000380300003800000004000A0000000000000000000A000000070000008DDABE00C7AB00000A000000000000000A000000000000000000803FCDCCCC3D00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000600000006000000010000001400000000008040000020410000000000008040000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011000000000000000000000000000000BA00000000000000C200000000000000300000000000000047000000000000005E0000000000000075000000000000008C00000000000000A300000000000000100014000000803F000000000000803F01000000040000100014000000803F0000803F0000803F03000000040000100014000000803F0000803F0000803F05000000040000100014000000803F0000803F0000803F07000000040000100014000000803F0000803F0000803F09000000040000100014000000803F000000000000803F0A0000000400000A00000000000000, ROWCOUNT = 3, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__FACULTAD__E6249CDEF868B45F]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[FACULTADES]([PK__FACULTAD__E6249CDEF868B45F]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 4, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__GRUPOS__A0BD40FF6A15FDC2]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[GRUPOS]([PK__GRUPOS__A0BD40FF6A15FDC2]) WITH STATS_STREAM = 0x01000000010000000000000000000000A3D3C7DB0000000040000000000000000000000000000000380300003800000004000A00000000000000000038000000, ROWCOUNT = 10, PAGECOUNT = 1
GO
/****** Object:  Statistic [_WA_Sys_00000002_04E4BC85]    Script Date: 26/05/2020 16:24:16 ******/
CREATE STATISTICS [_WA_Sys_00000002_04E4BC85] ON [dbo].[INSCRIPCION_X_ALUMNO]([alumnoID]) WITH STATS_STREAM = 0x010000000100000000000000000000009CA2AE0100000000CB010000000000008B0100000000000038038BDC3800000004000A00000000000000000001000000070000009548F400C7AB00002A000000000000002A00000000000000000000000000803F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000010000000100000014000000000080400000284200000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000001F00000000000000270000000000000008000000000000001000140000002842000000000000803F010000000400002A00000000000000
GO
/****** Object:  Statistic [PK_INSCRIPCION_X_ALUMNO]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[INSCRIPCION_X_ALUMNO]([PK_INSCRIPCION_X_ALUMNO]) WITH STATS_STREAM = 0x01000000010000000000000000000000B2D8E0560000000056040000000000001604000000000000380300003800000004000A00000000000000000000770073070000008607F600C7AB00002A000000000000002A000000000000000000803F310CC33C00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001600000016000000010000001400000000008040000028420000000000008040000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011000000000000000000000000000000AA02000000000000B202000000000000B000000000000000C700000000000000DE00000000000000F5000000000000000C0100000000000023010000000000003A01000000000000510100000000000068010000000000007F010000000000009601000000000000AD01000000000000C401000000000000DB01000000000000F2010000000000000902000000000000200200000000000037020000000000004E0200000000000065020000000000007C020000000000009302000000000000100014000000803F000000000000803F01000000040000100014000000803F0000803F0000803F03000000040000100014000000803F0000803F0000803F05000000040000100014000000803F0000803F0000803F07000000040000100014000000803F0000803F0000803F09000000040000100014000000803F0000803F0000803F0B000000040000100014000000803F0000803F0000803F0D000000040000100014000000803F0000803F0000803F0F000000040000100014000000803F0000803F0000803F11000000040000100014000000803F0000803F0000803F13000000040000100014000000803F0000803F0000803F15000000040000100014000000803F0000803F0000803F17000000040000100014000000803F0000803F0000803F19000000040000100014000000803F0000803F0000803F1B000000040000100014000000803F0000803F0000803F1D000000040000100014000000803F0000803F0000803F1F000000040000100014000000803F0000803F0000803F21000000040000100014000000803F0000803F0000803F23000000040000100014000000803F0000803F0000803F25000000040000100014000000803F0000803F0000803F27000000040000100014000000803F0000803F0000803F29000000040000100014000000803F000000000000803F2A0000000400002A00000000000000, ROWCOUNT = 42, PAGECOUNT = 1
GO
/****** Object:  Statistic [_WA_Sys_00000004_3F466844]    Script Date: 26/05/2020 16:24:16 ******/
CREATE STATISTICS [_WA_Sys_00000004_3F466844] ON [dbo].[MATERIAS]([carreraID]) WITH STATS_STREAM = 0x01000000010000000000000000000000BC9D6D2300000000CB010000000000008B010000000000003802803F3800000004000A00000000000000000000140000070000008565AA00C4AB00002E000000000000002E00000000000000000000000000803F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000010000000100000014000000000080400000384200000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000001F00000000000000270000000000000008000000000000001000140000003842000000000000803F060000000400002E00000000000000
GO
/****** Object:  Statistic [_WA_Sys_0000000A_3F466844]    Script Date: 26/05/2020 16:24:16 ******/
CREATE STATISTICS [_WA_Sys_0000000A_3F466844] ON [dbo].[MATERIAS]([pensumID]) WITH STATS_STREAM = 0x01000000010000000000000000000000E3109A2C00000000CB010000000000008B010000000000003802803F3800000004000A00000000000000000000140000070000008265AA00C4AB00002E000000000000002E00000000000000000000000000803F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000010000000100000014000000000080400000384200000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000001F00000000000000270000000000000008000000000000001000140000003842000000000000803F010000000400002E00000000000000
GO
/****** Object:  Statistic [_WA_Sys_0000000B_3F466844]    Script Date: 26/05/2020 16:24:16 ******/
CREATE STATISTICS [_WA_Sys_0000000B_3F466844] ON [dbo].[MATERIAS]([cicloID]) WITH STATS_STREAM = 0x01000000010000000000000000000000A5632C1A0000000066020000000000002602000000000000380200003800000004000A00000000000000000000000000070000008865AA00C4AB00002E000000000000002E000000000000003694573ECDCCCC3D00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000600000006000000010000001400000000008040000038420000000000008040000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011000000000000000000000000000000BA00000000000000C200000000000000300000000000000047000000000000005E0000000000000075000000000000008C00000000000000A300000000000000100014000000A040000000000000803F01000000040000100014000000A0400000A0400000A04003000000040000100014000000A0400000A0400000A04005000000040000100014000000A0400000A0400000A0400700000004000010001400000080400000804000008040090000000400001000140000004040000000000000803F0A0000000400002E00000000000000
GO
/****** Object:  Statistic [PK__MATERIAS__3BC06EBA0CC9E4CA]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[MATERIAS]([PK__MATERIAS__3BC06EBA0CC9E4CA]) WITH STATS_STREAM = 0x010000000100000000000000000000009C4C2AD900000000E515000000000000A515000000000000380300003800000004000A00000000000000000053006900070000009FF0EA00C7AB00004B010000000000004B010000000000000000803F74FE453B0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000A7000000A70000000100000014000000000080400080A54300000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000003914000000000000411400000000000038050000000000004F0500000000000066050000000000007D050000000000009405000000000000AB05000000000000C205000000000000D905000000000000F00500000000000007060000000000001E0600000000000035060000000000004C0600000000000063060000000000007A060000000000009106000000000000A806000000000000BF06000000000000D606000000000000ED0600000000000004070000000000001B0700000000000032070000000000004907000000000000600700000000000077070000000000008E07000000000000A507000000000000BC07000000000000D307000000000000EA07000000000000010800000000000018080000000000002F0800000000000046080000000000005D0800000000000074080000000000008B08000000000000A208000000000000B908000000000000D008000000000000E708000000000000FE0800000000000015090000000000002C0900000000000043090000000000005A09000000000000710900000000000088090000000000009F09000000000000B609000000000000CD09000000000000E409000000000000FB09000000000000120A000000000000290A000000000000400A000000000000570A0000000000006E0A000000000000850A0000000000009C0A000000000000B30A000000000000CA0A000000000000E10A000000000000F80A0000000000000F0B000000000000260B0000000000003D0B000000000000540B0000000000006B0B000000000000820B000000000000990B000000000000B00B000000000000C70B000000000000DE0B000000000000F50B0000000000000C0C000000000000230C0000000000003A0C000000000000510C000000000000680C0000000000007F0C000000000000960C000000000000AD0C000000000000C40C000000000000DB0C000000000000F20C000000000000090D000000000000200D000000000000370D0000000000004E0D000000000000650D0000000000007C0D000000000000930D000000000000AA0D000000000000C10D000000000000D80D000000000000EF0D000000000000060E0000000000001D0E000000000000340E0000000000004B0E000000000000620E000000000000790E000000000000900E000000000000A70E000000000000BE0E000000000000D50E000000000000EC0E000000000000030F0000000000001A0F000000000000310F000000000000480F0000000000005F0F000000000000760F0000000000008D0F000000000000A40F000000000000BB0F000000000000D20F000000000000E90F000000000000001000000000000017100000000000002E1000000000000045100000000000005C1000000000000073100000000000008A10000000000000A110000000000000B810000000000000CF10000000000000E610000000000000FD1000000000000014110000000000002B1100000000000042110000000000005911000000000000701100000000000087110000000000009E11000000000000B511000000000000CC11000000000000E311000000000000FA11000000000000111200000000000028120000000000003F1200000000000056120000000000006D1200000000000084120000000000009B12000000000000B212000000000000C912000000000000E012000000000000F7120000000000000E1300000000000025130000000000003C1300000000000053130000000000006A1300000000000081130000000000009813000000000000AF13000000000000C613000000000000DD13000000000000F4130000000000000B140000000000002214000000000000100014000000803F000000000000803F01000000040000100014000000803F0000803F0000803F03000000040000100014000000803F000000000000803F04000000040000100014000000803F0000803F0000803F06000000040000100014000000803F0000803F0000803F08000000040000100014000000803F0000803F0000803F0A000000040000100014000000803F0000803F0000803F0C000000040000100014000000803F0000803F0000803F0E000000040000100014000000803F0000803F0000803F10000000040000100014000000803F0000803F0000803F12000000040000100014000000803F0000803F0000803F14000000040000100014000000803F0000803F0000803F16000000040000100014000000803F0000803F0000803F18000000040000100014000000803F0000803F0000803F1A000000040000100014000000803F0000803F0000803F1C000000040000100014000000803F0000803F0000803F1E000000040000100014000000803F0000803F0000803F20000000040000100014000000803F0000803F0000803F22000000040000100014000000803F0000803F0000803F24000000040000100014000000803F0000803F0000803F26000000040000100014000000803F0000803F0000803F28000000040000100014000000803F0000803F0000803F2A000000040000100014000000803F0000803F0000803F2C000000040000100014000000803F0000803F0000803F2E000000040000100014000000803F0000803F0000803F30000000040000100014000000803F0000803F0000803F32000000040000100014000000803F0000803F0000803F34000000040000100014000000803F0000803F0000803F36000000040000100014000000803F0000803F0000803F38000000040000100014000000803F0000803F0000803F3A000000040000100014000000803F0000803F0000803F3C000000040000100014000000803F0000803F0000803F3E000000040000100014000000803F0000803F0000803F40000000040000100014000000803F0000803F0000803F42000000040000100014000000803F0000803F0000803F44000000040000100014000000803F0000803F0000803F46000000040000100014000000803F0000803F0000803F48000000040000100014000000803F0000803F0000803F4A000000040000100014000000803F0000803F0000803F4C000000040000100014000000803F0000803F0000803F4E000000040000100014000000803F0000803F0000803F50000000040000100014000000803F0000803F0000803F52000000040000100014000000803F0000803F0000803F54000000040000100014000000803F0000803F0000803F56000000040000100014000000803F0000803F0000803F58000000040000100014000000803F0000803F0000803F5A000000040000100014000000803F0000803F0000803F5C000000040000100014000000803F0000803F0000803F5E000000040000100014000000803F0000803F0000803F60000000040000100014000000803F0000803F0000803F62000000040000100014000000803F0000803F0000803F64000000040000100014000000803F0000803F0000803F66000000040000100014000000803F0000803F0000803F68000000040000100014000000803F0000803F0000803F6A000000040000100014000000803F0000803F0000803F6C000000040000100014000000803F0000803F0000803F6E000000040000100014000000803F0000803F0000803F70000000040000100014000000803F0000803F0000803F72000000040000100014000000803F0000803F0000803F74000000040000100014000000803F0000803F0000803F76000000040000100014000000803F0000803F0000803F78000000040000100014000000803F0000803F0000803F7A000000040000100014000000803F0000803F0000803F7C000000040000100014000000803F0000803F0000803F7E000000040000100014000000803F0000803F0000803F80000000040000100014000000803F0000803F0000803F82000000040000100014000000803F0000803F0000803F84000000040000100014000000803F0000803F0000803F86000000040000100014000000803F0000803F0000803F88000000040000100014000000803F0000803F0000803F8A000000040000100014000000803F0000803F0000803F8C000000040000100014000000803F0000803F0000803F8E000000040000100014000000803F0000803F0000803F90000000040000100014000000803F0000803F0000803F92000000040000100014000000803F0000803F0000803F94000000040000100014000000803F0000803F0000803F96000000040000100014000000803F0000803F0000803F98000000040000100014000000803F0000803F0000803F9A000000040000100014000000803F0000803F0000803F9C000000040000100014000000803F0000803F0000803F9E000000040000100014000000803F0000803F0000803FA0000000040000100014000000803F0000803F0000803FA2000000040000100014000000803F0000803F0000803FA4000000040000100014000000803F0000803F0000803FA6000000040000100014000000803F0000803F0000803FA8000000040000100014000000803F0000803F0000803FAA000000040000100014000000803F0000803F0000803FAC000000040000100014000000803F0000803F0000803FAE000000040000100014000000803F0000803F0000803FB0000000040000100014000000803F0000803F0000803FB2000000040000100014000000803F0000803F0000803FB4000000040000100014000000803F0000803F0000803FB6000000040000100014000000803F0000803F0000803FB8000000040000100014000000803F0000803F0000803FBA000000040000100014000000803F0000803F0000803FBC000000040000100014000000803F0000803F0000803FBE000000040000100014000000803F0000803F0000803FC0000000040000100014000000803F0000803F0000803FC2000000040000100014000000803F0000803F0000803FC4000000040000100014000000803F0000803F0000803FC6000000040000100014000000803F0000803F0000803FC8000000040000100014000000803F0000803F0000803FCA000000040000100014000000803F0000803F0000803FCC000000040000100014000000803F0000803F0000803FCE000000040000100014000000803F0000803F0000803FD0000000040000100014000000803F0000803F0000803FD2000000040000100014000000803F0000803F0000803FD4000000040000100014000000803F0000803F0000803FD6000000040000100014000000803F0000803F0000803FD8000000040000100014000000803F0000803F0000803FDA000000040000100014000000803F0000803F0000803FDC000000040000100014000000803F0000803F0000803FDE000000040000100014000000803F0000803F0000803FE0000000040000100014000000803F0000803F0000803FE2000000040000100014000000803F0000803F0000803FE4000000040000100014000000803F0000803F0000803FE6000000040000100014000000803F0000803F0000803FE8000000040000100014000000803F0000803F0000803FEA000000040000100014000000803F0000803F0000803FEC000000040000100014000000803F0000803F0000803FEE000000040000100014000000803F0000803F0000803FF0000000040000100014000000803F0000803F0000803FF2000000040000100014000000803F0000803F0000803FF4000000040000100014000000803F0000803F0000803FF6000000040000100014000000803F0000803F0000803FF8000000040000100014000000803F0000803F0000803FFA000000040000100014000000803F0000803F0000803FFC000000040000100014000000803F0000803F0000803FFE000000040000100014000000803F0000803F0000803F00010000040000100014000000803F0000803F0000803F02010000040000100014000000803F0000803F0000803F04010000040000100014000000803F0000803F0000803F06010000040000100014000000803F0000803F0000803F08010000040000100014000000803F0000803F0000803F0A010000040000100014000000803F0000803F0000803F0C010000040000100014000000803F0000803F0000803F0E010000040000100014000000803F0000803F0000803F10010000040000100014000000803F0000803F0000803F12010000040000100014000000803F0000803F0000803F14010000040000100014000000803F0000803F0000803F16010000040000100014000000803F0000803F0000803F18010000040000100014000000803F0000803F0000803F1A010000040000100014000000803F0000803F0000803F1C010000040000100014000000803F0000803F0000803F1E010000040000100014000000803F0000803F0000803F20010000040000100014000000803F0000803F0000803F22010000040000100014000000803F0000803F0000803F24010000040000100014000000803F0000803F0000803F26010000040000100014000000803F0000803F0000803F28010000040000100014000000803F0000803F0000803F2A010000040000100014000000803F0000803F0000803F2C010000040000100014000000803F0000803F0000803F2E010000040000100014000000803F0000803F0000803F30010000040000100014000000803F0000803F0000803F32010000040000100014000000803F0000803F0000803F34010000040000100014000000803F0000803F0000803F36010000040000100014000000803F0000803F0000803F38010000040000100014000000803F0000803F0000803F3A010000040000100014000000803F0000803F0000803F3C010000040000100014000000803F0000803F0000803F3E010000040000100014000000803F0000803F0000803F40010000040000100014000000803F0000803F0000803F42010000040000100014000000803F0000803F0000803F44010000040000100014000000803F0000803F0000803F46010000040000100014000000803F0000803F0000803F48010000040000100014000000803F0000803F0000803F4A010000040000100014000000803F000000000000803F4B0100000400004B01000000000000, ROWCOUNT = 331, PAGECOUNT = 5
GO
/****** Object:  Statistic [_WA_Sys_00000007_239E4DCF]    Script Date: 26/05/2020 16:24:16 ******/
CREATE STATISTICS [_WA_Sys_00000007_239E4DCF] ON [dbo].[MUNICIPIO]([departamentoID]) WITH STATS_STREAM = 0x010000000100000000000000000000001F3A290D000000005E030000000000001E03000000000000380300003800000004000A00000000000000000000000000070000008B62CA00C7AB00001E010000000000001E01000000000000000000002549923D00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000E0000000E00000001000000140000000000804000008F430000000000008040000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011000000000000000000000000000000B201000000000000BA01000000000000700000000000000087000000000000009E00000000000000B500000000000000CC00000000000000E300000000000000FA00000000000000110100000000000028010000000000003F0100000000000056010000000000006D0100000000000084010000000000009B01000000000000100014000000B041000000000000803F010000000400001000140000008841000000000000803F020000000400001000140000009041000000000000803F030000000400001000140000000842000000000000803F04000000040000100014000000B841000000000000803F05000000040000100014000000A041000000000000803F060000000400001000140000008841000000000000803F07000000040000100014000000B841000000000000803F080000000400001000140000002041000000000000803F090000000400001000140000006041000000000000803F0A000000040000100014000000C041000000000000803F0B000000040000100014000000A041000000000000803F0C000000040000100014000000D041000000000000803F0D0000000400001000140000009041000000000000803F0E0000000400001E01000000000000
GO
/****** Object:  Statistic [PK__MUNICIPI__CBC36A2133ADEE64]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[MUNICIPIO]([PK__MUNICIPI__CBC36A2133ADEE64]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 286, PAGECOUNT = 3
GO
/****** Object:  Statistic [PK__NOTAS__0ADFDACACC34AB09]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[NOTAS]([PK__NOTAS__0ADFDACACC34AB09]) WITH STATS_STREAM = 0x01000000010000000000000000000000A3D3C7DB0000000040000000000000000000000000000000380300003800000004000A00000000000000000038000000, ROWCOUNT = 42, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__PAIS__45785BAB7F065139]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[PAIS]([PK__PAIS__45785BAB7F065139]) WITH STATS_STREAM = 0x01000000010000000000000000000000A3D3C7DB0000000040000000000000000000000000000000380300003800000004000A00000000000000000038000000, ROWCOUNT = 70, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK_PENSUM]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[PENSUM]([PK_PENSUM]) WITH STATS_STREAM = 0x0100000001000000000000000000000002217CE200000000CB010000000000008B01000000000000380300003800000004000A00000000000000000000740061070000008365AA00C4AB000001000000000000000100000000000000000000000000803F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000010000000100000014000000000080400000803F00000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000001F0000000000000027000000000000000800000000000000100014000000803F000000000000803F010000000400000100000000000000, ROWCOUNT = 7, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__PERSONAL__27515FB067454BBF]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[PERSONAL_ADMINISTRATIVO]([PK__PERSONAL__27515FB067454BBF]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 0, PAGECOUNT = 0
GO
/****** Object:  Statistic [PK__PUESTO_A__2BAB30826B8EB4FD]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[PUESTO_ADMINISTRATIVO]([PK__PUESTO_A__2BAB30826B8EB4FD]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 0, PAGECOUNT = 0
GO
/****** Object:  Statistic [PK_TIPO_CARNET]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[TIPO_CARNET]([PK_TIPO_CARNET]) WITH STATS_STREAM = 0x010000000100000000000000000000003147D4B900000000EA01000000000000AA01000000000000380300613800000004000A0000000000000000000000000007000000A0CC0D01C7AB0000030000000000000003000000000000000000803FABAAAA3E000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000000020000000100000014000000000080400000404000000000000080400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000110000000000000000000000000000003E00000000000000460000000000000010000000000000002700000000000000100014000000803F000000000000803F01000000040000100014000000803F0000803F0000803F030000000400000300000000000000, ROWCOUNT = 3, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK_TIPO_DOCUMENTOS]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[TIPO_DOCUMENTOS]([PK_TIPO_DOCUMENTOS]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 7, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__TIPO_ING__CAF27F1BDB056BA4]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[TIPO_INGRESO]([PK__TIPO_ING__CAF27F1BDB056BA4]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 3, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__TIPO_USU__12583A4384546ED0]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[TIPO_USUARIO]([PK__TIPO_USU__12583A4384546ED0]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 4, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__UNIVERSI__77BB8896034860AD]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[UNIVERSIDAD]([PK__UNIVERSI__77BB8896034860AD]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 1, PAGECOUNT = 1
GO
/****** Object:  Statistic [PK__USUARIOS__A5B1ABAEB7C50F75]    Script Date: 26/05/2020 16:24:16 ******/
UPDATE STATISTICS [dbo].[USUARIOS]([PK__USUARIOS__A5B1ABAEB7C50F75]) WITH STATS_STREAM = 0x01000000010000000000000000000000ED0358EE0000000040000000000000000000000000000000380300003800000004000A00000000000000000000000000, ROWCOUNT = 4, PAGECOUNT = 1
GO
USE [master]
GO
ALTER DATABASE [DBUCAD] SET  READ_WRITE 
GO
